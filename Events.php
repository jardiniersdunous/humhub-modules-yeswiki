<?php
/**
 * YesWiki
 * @link https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki
 * @license https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki/blob/main/docs/LICENCE.md
 * @author [Marc Farré](https://marc.fun)
 */

namespace humhub\modules\yeswiki;

use humhub\commands\CronController;
use humhub\components\console\Application;
use humhub\libs\Html;
use humhub\modules\admin\permissions\ManageSettings;
use humhub\modules\admin\permissions\ManageUsers;
use humhub\modules\admin\widgets\AdminMenu;
use humhub\modules\space\models\Membership;
use humhub\modules\space\models\Space;
use humhub\modules\ui\menu\MenuLink;
use humhub\modules\user\models\Group;
use humhub\modules\user\models\Profile;
use humhub\modules\user\models\User;
use humhub\modules\yeswiki\commands\YeswikiController;
use humhub\modules\yeswiki\models\UserNotExtended;
use humhub\modules\yeswiki\models\yeswiki\YesWikiPages;
use humhub\modules\yeswiki\models\yeswiki\YesWikiTriples;
use humhub\modules\yeswiki\models\yeswiki\YesWikiUsers;
use Throwable;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\NotSupportedException;
use yii\db\AfterSaveEvent;
use yii\helpers\BaseConsole;
use yii\helpers\Url;

// Don't use user\models\User as it will generate an event and execute onModelProfileAfterUpdate again !

/**
 * Profile module event handling.
 */
class Events
{
    /**
     * @param $event
     * @return void
     */
    public static function onConsoleApplicationInit($event)
    {
        /** @var Application $application */
        $application = $event->sender;
        $application->controllerMap['yeswiki'] = YeswikiController::class;
    }

    /**
     * If a user creates a new account, methods will be executed in this order :
     * 1. onModelSpaceMembershipMemberAdded() : User will be added to YesWiki's groups when added as member of default space
     * 2. onModelGroupUserAfterInsert() : User and profile will be created in YesWiki when added to a group
     * 3. onModelProfileAfterInsert() : Username and profile will be updated
     * 4. onFormAfterRegistration()
     **/
    /**
     * @throws Throwable
     */
    public static function onAdminMenuInit($event)
    {
        /** @var AdminMenu $menu */
        $menu = $event->sender;

        /** @var Module $module */
        $module = Yii::$app->getModule('yeswiki');

        if (Yii::$app->user->can(ManageSettings::class)) { // Don't move in 'isVisible' as it doesn't work in all cases and because the "if" costs less
            $menu->addEntry(new MenuLink([
                'label' => $module->getName(),
                'icon' => $module->icon,
                'sortOrder' => 1101,
                'isActive' => MenuLink::isActiveState('yeswiki', 'admin'),
                'url' => Url::to(['/yeswiki/admin/index']),
                'isVisible' => true,
            ]));
        }
    }

    /**
     * Update age range
     * @param $event
     * @return void
     * @throws \Exception
     */
    public static function onCronDailyRun($event)
    {
        if (!Yii::$app->getModule('ecommerce')) {
            return;
        }

        /** @var CronController $controller */
        $controller = $event->sender;
        $controller->stdout("YesWiki module: Updating profile fields...");

        /** @var UserNotExtended $userNotExtended */
        foreach (UserNotExtended::find()->where(['status' => User::STATUS_ENABLED])->each(500) as $userNotExtended) {
            if ($userNotExtended->profile->hasAttribute('tranche_age')) {
                $oldAgeRange = $userNotExtended->profile->tranche_age;
                $userNotExtended->updateProfile(true);
                if ($oldAgeRange !== $userNotExtended->profile->tranche_age) {
                    YesWikiPages::addOrUpdateProfileInAllSites($userNotExtended);
                }
            }
        }

        $controller->stdout('done.' . PHP_EOL, BaseConsole::FG_GREEN);
    }


    // Updated username and profile
    public static function onModelProfileAfterInsert($event)
    {
        // Caution: don't use Yii::$app->user because if it's a new user, it's not yet created!

        if (!isset($event->sender)) {
            return;
        }

        // Create HumHub profile var
        /** @var Profile $profile */
        $profile = $event->sender;

        // Check if required profile fields exists
        if (empty($profile->user_id) || empty($profile->birthday)) {
            return;
        }

        // Check if special values (if user created by REST API)
        if ($profile->birthday === '1900-01-01') {
            $profile->birthday = null;
            $profile->zip = null;
            $profile->country = null;
            $profile->save(false);
            return;
        }

        // Get user in database, but not \humhub\modules\user\models\User otherwise it will create each time a new \humhub\modules\user\models\Profile::EVENT_AFTER_UPDATE event
        $userNotExtended = UserNotExtended::findOne(['id' => $profile->user_id]);
        if ($userNotExtended === null) {
            return;
        }

        // Update HumHub profile
        $userNotExtended->updateProfile();

        // Update username from first and last name, checking if username is available in all YesWiki sites
        $userNotExtended->checkUsernameAvailabilityAndUpdateAllSites();

        // Now, onFormAfterRegistration() will be executed
    }


    // Each time profile is updated or on user delete (after onModelUserBeforeDelete)
    public static function onModelProfileAfterUpdate($event)
    {
        // Caution: don't use Yii::$app->user because if it's a new user, it's not yet created !

        if (!isset($event->sender)) {
            return;
        }

        // Create HumHub profile var
        $profile = $event->sender;

        // Get user in database, but not \humhub\modules\user\models\User otherwise it will create each time a new \humhub\modules\user\models\Profile::EVENT_AFTER_UPDATE event
        $userNotExtended = UserNotExtended::findOne(['id' => $profile->user_id]);
        if ($userNotExtended === null || $userNotExtended->status !== User::STATUS_ENABLED) {
            return;
        }

        // Update HumHub profile (must be done BEFORE YesWikiPages::addOrUpdateProfileInAllSites())
        $userNotExtended->updateProfile();

        // Update username from first and last name, checking if username is available in all sites
        // Update YesWiki usernames in YesWiki sites, but doesn't add YesWiki user or profile as it just a profile update
        $userNotExtended->checkUsernameAvailabilityAndUpdateAllSites();

        // Update YesWiki profile fields (checkUsernameAvailabilityAndUpdateAllSites() must be done AFTER because in the body field we write the displayName and the username)
        // If new account creation, will be done a second time in `onFormAfterRegistration` because profile values are not saved here
        YesWikiPages::addOrUpdateProfileInAllSites($userNotExtended);
    }


    // Is user is deleted, remove user in YesWiki tables
    public static function onModelUserBeforeDelete($event)
    {
        if (!isset($event->sender)) {
            return;
        }

        // Get user
        $user = $event->sender;

        // Get user for events
        $userNotExtended = UserNotExtended::findOne(['id' => $user->id]);
        if ($userNotExtended === null) {
            return;
        }

        // Delete user in YesWiki
        YesWikiUsers::deleteUserAndContentInAllSites($userNotExtended);
    }


    /**
     * If user email has changed in HumHub, update it on YesWiki
     * @param AfterSaveEvent $event
     * @throws NotSupportedException
     */
    public static function onModelUserAfterUpdate($event)
    {
        if (!isset($event->sender, $event->changedAttributes)) {
            return;
        }

        /** @var User $user */
        $user = $event->sender;

        // Get changed attributes
        $changedAttributes = $event->changedAttributes;

        if (array_key_exists('email', $changedAttributes)) {
            $userNotExtended = UserNotExtended::findOne($user->id);
            YesWikiUsers::updateEmailInAllSites($userNotExtended);
        }
    }


    // When a new user register, onFormAfterRegistration is executed just after onModelProfileChange() (after the user has successfully validated the registration form)
    public static function onFormAfterRegistration(yii\web\UserEvent $event)
    {
        // Here, onModelProfileAfterInsert($event) has just been executed

        // Do not store on console request
        if (Yii::$app->request->isConsoleRequest) {
            return;
        }

        // Get user
        $userId = $event->identity->id;
        $user = User::findOne(['id' => $userId]); // Yii::$app->user doesnt work here
        if ($user === null) {
            return;
        }
        $userNotExtended = UserNotExtended::findOne(['id' => $userId]);

        // Update profile in YesWiki tables as first `addOrUpdateProfileInAllSites` done in `onModelProfileAfterUpdate` doesn't save profile values.
        YesWikiPages::addOrUpdateProfileInAllSites($userNotExtended);
    }

    public static function onModelGroupUserAfterInsert($event)
    {
        if (!isset($event->sender)) {
            return;
        }

        // Create HumHub groupUser, group and user
        $groupUser = $event->sender;
        $group = $groupUser->group;
        $user = $groupUser->user;

        // Get user for events
        $userNotExtended = UserNotExtended::findOne(['id' => $user->id]);
        if ($userNotExtended === null) {
            return;
        }

        // Check username availability in this new YesWiki
        $userNotExtended->checkUsernameAvailabilityAndUpdateAllSites();

        // Add user and profile in this new YesWiki
        YesWikiUsers::addUserAndProfile($group, $userNotExtended);

        // Add user in YesWiki's groups
        foreach ($userNotExtended->spaces as $space) {
            // Add user in YesWiki group (and create group if doesn't exist)
            YesWikiTriples::addMemberInGroup($group, $space, $userNotExtended->username);

            // If is admin
            $membership = $space->getMembership($userNotExtended->id);
            if ($membership && $membership->group_id === Space::USERGROUP_ADMIN) {
                // Add as admin
                YesWikiTriples::addMemberInGroup($group, $space, $userNotExtended->username, true);
            }
        }
    }


    public static function onModelGroupUserAfterDelete($event)
    {
        if (!isset($event->sender)) {
            return;
        }

        // Create HumHub group
        $groupUser = $event->sender;
        $group = $groupUser->group;
        $user = $groupUser->user;

        // Get user for events
        $userNotExtended = UserNotExtended::findOne(['id' => $user->id]);
        if ($userNotExtended === null) {
            return;
        }

        // Delete user and profile in YesWiki
        YesWikiUsers::deleteUserAndContent($group, $userNotExtended);

        // Remove user in YesWiki groups
        foreach ($userNotExtended->spaces as $space) {
            YesWikiTriples::removeMemberInGroup($group, $space, $userNotExtended->username);
            YesWikiTriples::removeMemberInGroup($group, $space, $userNotExtended->username, true);
        }
    }




    // *** SPACE ***


    // Adds the space creator as onModelSpaceMembershipMemberAdded() is not executed for creator
    public static function onModelSpaceAfterInsert($event)
    {
        if (!isset($event->sender)) {
            return;
        }

        // Get space
        $space = $event->sender;
        // Get space with jdn functions
        $space = Space::findOne(['id' => $space->id]);

        // For each space creator's groups
        foreach (Yii::$app->user->identity->groups as $group) {

            // Adds the space creator (current user) to YesWiki group members (group will be created if not exists)
            YesWikiTriples::addMemberInGroup($group, $space, Yii::$app->user->identity->username);
            YesWikiTriples::addMemberInGroup($group, $space, Yii::$app->user->identity->username, true);
        }
    }


    public static function onModelSpaceBeforeDelete($event)
    {
        if (empty($event->sender)) {
            return;
        }

        /** @var Space $space */
        $space = $event->sender;

        // For each group (space members have already been deleted)
        /** @var Group $group */
        foreach (Group::find()->each() as $group) {
            // Delete group
            YesWikiTriples::deleteGroup($group, $space);
            YesWikiTriples::deleteGroup($group, $space, true);
        }
    }


    public static function onModelSpaceBeforeUpdate($event)
    {
        if (!isset($event->sender)) {
            return;
        }

        // Get new space
        /** @var Space $newSpace */
        $newSpace = $event->sender;

        // Get old HumHub space
        $oldSpace = Space::findOne(['id' => $newSpace->id]);

        // If space status has changed
        // Status change is not implemented in humhub 1.5.1

        // If space name has changed
        if ($oldSpace->url !== $newSpace->url) {
            $groups = [];
            /** @var Membership $membership */
            foreach ($oldSpace->getMemberships()->each() as $membership) {
                foreach ($membership->user->groups as $group) {
                    $groups[$group->id] = $group;
                }
            }
            // For each group that space members have
            foreach ($groups as $group) {
                // Update groups name
                YesWikiTriples::updateGroupName($group, $oldSpace, $newSpace);
                YesWikiTriples::updateGroupName($group, $oldSpace, $newSpace, true);
            }
        }
    }


    public static function onModelSpaceMembershipMemberAdded($event)
    {
        if (!isset($event)) {
            return;
        }

        // Get HumHub user and space
        /** @var Membership $membership */
        $membership = $event; // not $event->sender as it is executed by queue/run
        $userNotExtended = UserNotExtended::findOne(['id' => $membership->user->id]);
        $space = Space::findOne(['id' => $membership->space->id]);

        // Add member to YesWiki groups
        foreach ($userNotExtended->syncGroups as $group) {

            // Add user to YesWiki group members (group will be created if not exists)
            YesWikiTriples::addMemberInGroup($group, $space, $userNotExtended->username);
            // Don't add as admin as a member can become admin only after being added ; managed by onModelSpaceMembershipAfterUpdate()
        }
    }


    public static function onModelSpaceMembershipMemberRemoved($event)
    {
        if (!isset($event)) {
            return;
        }

        // Get HumHub user and space
        /** @var Membership $membership */
        $membership = $event;
        $userNotExtended = UserNotExtended::findOne(['id' => $membership->user->id]);
        $space = Space::findOne(['id' => $membership->space->id]);

        // Remove member from YesWiki groups
        foreach ($userNotExtended->syncGroups as $group) {
            YesWikiTriples::removeMemberInGroup($group, $space, $userNotExtended->username);
            YesWikiTriples::removeMemberInGroup($group, $space, $userNotExtended->username, true);
        }
    }


    public static function onModelSpaceMembershipAfterUpdate($event)
    {
        if (!isset($event->sender)) {
            return;
        }

        // Get HumHub membership
        /** @var Membership $membership */
        $membership = $event->sender;
        // Get membership with jdn functions
        $membership = Membership::findOne(['id' => $membership->id]);

        $user = $membership->user;
        $space = $membership->space;

        foreach ($user->groups as $group) {
            if ($membership->group_id === 'admin') {
                // Add member to YesWiki admin group
                YesWikiTriples::addMemberInGroup($group, $space, $user->username, true);
            } else {
                // Remove member from YesWiki admin group
                YesWikiTriples::removeMemberInGroup($group, $space, $user->username, true);
            }
        }
    }


    /**
     * @throws Throwable
     * @throws InvalidConfigException
     */
    public static function onSpaceAdminMenuInit($event)
    {
        /** @var \humhub\modules\space\widgets\HeaderControlsMenu $menu */
        $menu = $event->sender;

        $space = $menu->space;
        if (
            $space->isAdmin()
            || (!Yii::$app->user->isGuest && Yii::$app->user->can(ManageUsers::class))
        ) {
            $yesWikiGroupNameForMembers = '@' . YesWikiTriples::groupNameFromHumhubSpace($space);
            $yesWikiGroupNameForAdmins = '@' . YesWikiTriples::groupNameFromHumhubSpace($space, true);

            $menu->addEntry(new MenuLink([
                'label' => '
                    <span class="tt" title="Ce nom est celui du groupe YesWiki correspondant à cet espace. Tous les membres de cet espace sont dans ce groupe du YesWiki correspondant à leur(s) rôle(s). Cliquer pour copier dans le presse papier">
                        Groupe YesWiki des membres :
                        <input type="text" value="' . $yesWikiGroupNameForMembers . '" id="yeswiki-group-name-members">
                    </span>
                    <script ' . Html::nonce() . '>
                        $("a[data-menu-id=\'copy-yeswiki-group-name-members\']").on("click", function() {
                            var copyText = document.getElementById("yeswiki-group-name-members");
                            copyText.select();
                            copyText.setSelectionRange(0, 99999); /*For mobile devices*/
                            document.execCommand("copy");
                            alert("' . $yesWikiGroupNameForMembers . ' a été copié dans votre presse papier, vous pouvez le coller où vous voulez !");
                        });
                    </script>
                    ',
                'id' => 'copy-yeswiki-group-name-members',
                'url' => '#',
                'icon' => '<i class="fa fa-users tt" title="Groupe YesWiki des membres correspondant à cet espace : ' . $yesWikiGroupNameForMembers . ' | Cliquer pour copier dans le presse papier."  style="cursor: pointer;"></i>',
                'isActive' => MenuLink::isActiveState('jdn', 'container', 'view'),
            ]));

            $menu->addEntry(new MenuLink([
                'label' => '
                    <span class="tt" title="Ce nom est celui du groupe YesWiki correspondant à cet espace. Tous les administrateurs de cet espace sont dans ce groupe du YesWiki correspondant à leur(s) rôle(s). Cliquer pour copier dans le presse papier">
                        Groupe YesWiki des admins :
                        <input type="text" value="' . $yesWikiGroupNameForAdmins . '" id="yeswiki-group-name-admins">
                    </span>
                    <script ' . Html::nonce() . '>
                        $("a[data-menu-id=\'copy-yeswiki-group-name-admins\']").on("click", function() {
                            var copyText = document.getElementById("yeswiki-group-name-admins");
                            copyText.select();
                            copyText.setSelectionRange(0, 99999); /*For mobile devices*/
                            document.execCommand("copy");
                            alert("' . $yesWikiGroupNameForAdmins . ' a été copié dans votre presse papier, vous pouvez le coller où vous voulez !");
                        });
                    </script>
                    ',
                'id' => 'copy-yeswiki-group-name-admins',
                'url' => '#',
                'icon' => '<i class="fa fa-users tt" title="Groupe YesWiki des admins correspondant à cet espace : ' . $yesWikiGroupNameForAdmins . ' | Cliquer pour copier dans le presse papier."  style="cursor: pointer;"></i>',
                'isActive' => MenuLink::isActiveState('jdn', 'container', 'view'),
            ]));
        }
    }
}
