<?php
/**
 * YesWiki
 * @link https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki
 * @license https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki/blob/main/docs/LICENCE.md
 * @author [Marc Farré](https://marc.fun)
 */

namespace humhub\modules\yeswiki;

class Module extends \humhub\components\Module
{
    /**
     * @var string defines path for resources, including the screenshots path for the marketplace and module_image.png
     */
    public $resourcesPath = 'resources';

    /**
     * @var string defines the icon
     */
    public $icon = 'y-combinator';

    /**
     * @var int
     */
    public $yesWikiProfileFormId = 1000;

    /**
     * @var bool
     */
    public $profileFieldYesWikiLatLng = false;

    /**
     * Additional profile fields for YesWikiPages
     * @var array
     */
    public $extraProfileFieldsHumhub2YesWiki = [];

    /**
     * Additional profile values for YesWikiPages::PROFILE_VAL_HUMHUB_2_YESWIKI
     * @var array
     */
    public $extraProfileValHumhub2YesWiki = [];
}
