<?php
/**
 * YesWiki
 * @link https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki
 * @license https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki/blob/main/docs/LICENCE.md
 * @author [Marc Farré](https://marc.fun)
 */

namespace humhub\modules\yeswiki\commands;


use humhub\modules\user\models\User;
use humhub\modules\yeswiki\models\UserNotExtended;
use humhub\modules\yeswiki\models\yeswiki\YesWikiPages;
use yii\console\Controller;
use yii\helpers\BaseConsole;

/**
 * To execute a command: php yii jdn/action-name
 */
class YeswikiController extends Controller
{
    /**
     * php yii yeswiki/update-age-ranges
     * @return void
     * @throws \Exception
     */
    public function actionUpdateAgeRanges()
    {
        /** @var UserNotExtended $userNotExtended */
        foreach (UserNotExtended::find()->where(['status' => User::STATUS_ENABLED])->each(500) as $userNotExtended) {
            if ($userNotExtended->profile->hasAttribute('tranche_age')) {
                $oldAgeRange = $userNotExtended->profile->tranche_age;
                $userNotExtended->updateProfile(true);
                if ($oldAgeRange !== $userNotExtended->profile->tranche_age) {
                    YesWikiPages::addOrUpdateProfileInAllSites($userNotExtended);
                }
            }
        }
        $this->stdout(PHP_EOL . 'End' . PHP_EOL, BaseConsole::FG_GREEN);
    }

    /**
     * php yii yeswiki/update-yes-wiki-profiles
     * @return void
     */
    public function actionUpdateYesWikiProfiles()
    {
        foreach (UserNotExtended::find()->where(['status' => User::STATUS_ENABLED])->each(500) as $userNotExtended) {
            YesWikiPages::addOrUpdateProfileInAllSites($userNotExtended);
        }
        $this->stdout(PHP_EOL . 'End' . PHP_EOL, BaseConsole::FG_GREEN);
    }
}