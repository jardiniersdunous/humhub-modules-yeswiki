Changelog
=========

1.5 (March 25, 2024)
--------------------
- Fix: Check if user is active before sync with YesWiki
- Chg: `Group Advanced` module replaced with `Classified Space` and `Category Group`

1.4 (October 16, 2023)
--------------------
- Fix: Compatibility with Members Map module v2.3.5
- Fix: Profile update for `tranche_age` and `tranche_age` no longer working on HumHub 1.15
- Enh: Added age range update every day
- Enh: Added username parsing with Name Parser module
- Fix: Test if username is valid using Name Parser module

1.3 (June 24, 2023)
--------------------
- Enh: Add `/yeswiki/admin/parse-user?id=xxx` to update profile, parse username and update username in all sites
- Fix: On email change, change the email on all YesWiki
- Fix: Added Html::nonce() to script tags for HumHub 1.15 (https://github.com/humhub/documentation/pull/84/files)
- Chg: In the `triples` table name, delete all entries related to a user only if the user is deleted or removed from a HumHub group. If the name changes or in case of new full sync with HumHub, delete only the entries related to the properties recreated (especially, keep the entries with property `https://yeswiki.net/vocabulary/progress`)

1.2.1 (Jun 1, 2023)
--------------------
- Fix: On user creation without invitation, `$userNotExtended->updateProfile()`
  and `$userNotExtended->checkUsernameAvailabilityAndUpdateAllSites()` where not triggered, which was leaving the Keycloak ID as username instead of parsing it from the first and last names.

1.2 (January 21, 2023)
--------------------
- Enh: Les noms des groupes YesWiki se base sur l'URL des espaces HumHub (au lieu du nom des espaces)

1.1 (January 16, 2023)
--------------------
- Enh: La synchronisation YW-HH se base sur un champ au niveau des groupes

1.0 (January 10, 2023)
--------------------
- Enh: First release !
