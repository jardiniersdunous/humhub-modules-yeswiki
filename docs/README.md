Module YesWiki
==========================

## Description

Synchronisation des utilisateurs, profils et groupes avec YesWiki :
- à chaque groupe HumHub (HH) il peut y avoir un site YesWiki (YW) lié (le nom des tables YW est déterminé dans la configuration)
- à chaque membre d'un groupe HH correspond un utilisateur et un profil dans le site YW correspondant au groupe HH
- à chaque membre d'un espace HH correspond un groupe YW dans chacun des sites YW correspondants aux groupes HH du membre
- Fonction cachée : `/yeswiki/admin/parse-user?id=xxx` to update profile, parse username and update username in all sites

## Fonctionnement de la synchronisation des comptes entre HumHub et les différents sites YesWiki

Le site [https://login.lescommuns.org/](https://login.lescommuns.org/) (Keycloak, hébergé sur le serveur des Communs) gère l’authentification pour permettre à un utilisateur de se connecter sur les sites des Jardiniers du Nous.

Ce serveur d’authentification SSO gère un premier niveau de comptes utilisateurs qui s’appuie sur ces informations principales :
*mail*, *prénom*, *nom* et *mot de passe*.

Cette page présente les mécanismes de synchronisation mis en place afin que les utilisateurs et droits définis au sein de HumHub puissent être utilisés au sein des différents sites YesWiki de la plateforme numérique des JdN. Nous prendrons d’abord le cas de la synchronisation effectuée pour le yeswiki « site vitrine ».

### Synchronisation des utilisateurs

Pour chaque utilisateur HumHub, un utilisateur sous YesWiki est associé. Ils partagent alors le même identifiant et à chaque création, modification ou suppression sur HumHub, la mise à jour est répercutée sur YesWiki.

L’identifiant qui leur est attribué est un identifiant unique de type YesWiki construit à partir du prénom et du nom. Par exemple, l’utilisateur « Daniel Durant » se verra attribuer l’identifiant « DanielDurand » s’il est disponible (pour plus d’information, cf le paragraphe « Avancé : Comment sont créés les identifiants ? ».\
\
Si un utilisateur modifie son nom ou prénom sur HumHub, son identifiant sera mise à jour au niveau de YesWiki et il gardera l’ensemble de ses droits qu’il avait dans YesWiki.

### Synchronisation du profil utilisateur

Les profils utilisateurs, c'est-à-dire l’ensemble des informations renseignées par un utilisateur lors de son inscription, sont également synchronisées dans le sens HumHub -> YesWiki lors d’un ajout/une modification/une suppression d’un utilisateur.

Chaque profil est alors enregistré sur une fiche de YesWiki (page particulière qui suit la structure d’un formulaire, et dans ce cas le formulaire n°1000) et porte le nom de l’identifiant donné à l’utilisateur.\
\
Avec cet identifiant (par exemple « DanielDurand »), il est ainsi facile de retrouver la visualisation du profil d’un utilisateur sur chacune des applications :

* sur HumHub, l’adresse est
  *https://grandjardin.jardiniersdunous.org/u/\[Identifiant\]/user/profile/about*. Exemple avec [DanielDurand](https://grandjardin.jardiniersdunous.org/u/DanielDurand/user/profile/about).
* sur YesWiki, l’adresse est
  *https://www.jardiniersdunous.org/?\[Identifiant\]*. Exemple avec [DanielDurand](https://www.jardiniersdunous.org/?DanielDurand).

Ces informations utilisateur ne devront être modifiées uniquement dans HumHub puisque c’est lui qui met à jour les informations sur YesWiki.

### Synchronisation des espaces HumHub avec les groupes YesWiki

Afin de pouvoir donner des droits particuliers aux pages YesWiki en fonction des espaces auxquels adhère un utilisateur, chaque liste des membres des espaces sous HumHub est synchronisée dans un groupe YesWiki. Encore une fois, cette synchronisation se fait uniquement dans le sens HumHub -> YesWiki lors d’un ajout ou la suppression d’un utilisateur.

Pour chaque espace HumHub, deux groupes YesWiki sont créés et mis à jour :

* le premier réunit l’ensemble des membres de l’espace et le groupe prend comme nom un identifiant unique créé à partir du préfixe humhub- et de l'URL de l’espace. Par exemple, les utilisateurs de l’espace « Outils numériques » sur HumHub seront réunis dans le groupe « humhub-outils-numeriques » dans YesWiki.
* le deuxième groupe se restreint uniquement aux administrateurs de l’espace. Celui-ci comporte le même nom que le groupe ci-dessus, mais avec « -admins » à la fin. Par exemple, l’espace « Cercle Général JdN » deviendra le groupe « humhub-outils-numeriques-admins » dans YesWiki.

A chaque fois qu’un espace est ajouté ou supprimé, cela mettra à jour la liste des groupes sur YesWiki. Mais également si un espace change d'URL, le nom de son groupe sera mise à jour et ce groupe gardera l’ensemble de ses droits dans YesWiki.\
\
Les fiches Yeswiki de profil des utilisateurs ne seront accessibles en lecture qu’aux groupes correspondant aux espaces HumHub dont il fait partie (rappel : en écriture, les utilisateurs ne pourront pas mettre à jour leur profil, ils devront forcément passer par HumHub).

### Avancé : Comment sont créés les identifiants ?

Les identifiants présentés ci-dessus suivent la méthode utilisée par YesWiki pour déterminer le nom unique d’une page. Ils sont créés à partir d’une donnée de l’entité en question : pour les utilisateurs, c’est la chaîne de caractère construite tel que «\[prénom\] \[nom\]» ; pour les groupes, c’est l'URL de l’espace HumHub associé.

L’identifiant est construit en deux étapes :

#### 1. une normalisation

On fait cette série de traitements sur la chaîne de caractère : on remplace toute ponctuation, tiret, apostrophe… (grosso modo, tout caractère spécial) par un espace, on passe tout en minuscule, on met chaque première lettre de chaque mot en majuscule, on enlève tous les espaces, on tronque le mot à 48 caractères s’il en comporte plus.

Par exemple, l’utilisatrice dont le prénom et nom donnent « Anaïs d’Arté-Monga » sera normalisée en « AnaisDArteMonga ».

#### 2. une vérification de son unicité

Dans un deuxième temps, on vérifie si le nom est disponible suivant certains critères. S’il ne l’est pas, on réessaye avec le même nom avec un ‘2’ derrière, et ainsi de suite ‘3’, ‘4’, jusqu’au moment où le nom est disponible.

* pour que l’identifiant d’un utilisateur soit disponible, il faudra qu’il n’y ait aucun utilisateur existant avec ce nom sur HumHub ainsi que sur YesWiki, et qu’il n’y ait pas non plus de page YesWiki qui ait cet identifiant comme nom (afin que la page de profil puisse être accessible à la page qui porte le nom de cet identifiant). Par exemple, s’il existe déjà une utilisatrice dont l’identifiant est AnaisDarteMonga, une autre personne nommée « Anais d’Arte Monga » se verra donner l’identifiant « AnaisDarteMonga2 ».
* pour les groupes, on ne testera pas leur unicité, ainsi le nom normalisé sera pris comme identifiant. S’il existe déjà un groupe avec cet identifiant sur yeswiki, on gardera les utilisateurs déjà présents dans ce groupe et l’ajout ou suppression d’autres utilisateurs à ce groupe lors des synchronisations avec HumHub n’impacteront pas leur présence à ce groupe.

### Avancé : Une synchronisation spécifique par rôle

Pour mieux comprendre comment sont synchronisés les utilisateurs et les groupes, il faut comprendre qu’à chaque rôle on peut associer une synchronisation à un yeswiki spécifique. C’est alors l’ensemble des utilisateurs associés à ce rôle et l’ensemble des espaces dont il fait partie qui sont synchronisés sur ce wiki. Ces synchronisations s’effectuent directement en allant écrire dans les tables du yeswiki concerné.

Par convention, les utilisateurs et groupes d’un rôle sont synchronisés dans le yeswiki dont les tables ont pour préfixe : «
*nomdurole_idrole__* » avec
*nomdurole* étant le nom normalisé du rôle (cf. section sur les identifiants) mais cette fois tout en minuscule. La synchronisation s’effectue uniquement si ces tables existent, pour certains rôles ces tables n’existeront pas et il n’y aura alors aucune synchronisation des utilisateurs et groupes liés au rôle.

Par exemple pour le rôle « Jardinier·e », c’est l’ensemble des utilisateurs et des espaces auxquels ils appartiennent qui seront synchronisés (cf. « Synchronisation des espaces HumHub avec les groupes YesWiki » qui expliquent comment sont créés les groupes yeswiki correspondants aux membres et administrateurs des espaces) :

* par convention les informations sont écrites dans les tables qui commencent par « jardiniere_2__ »
* chaque utilisateur est synchronisé sur le yeswiki site vitrine et un identifiant lui est attribué tel qu’expliqué dans la section « Avancé : Comment sont créés les identifiants ? »
* une page de profil est créé avec cet identifiant et cette page est précisément une fiche bazar dont l’« id_typeannonce » est fixé par convention à 1000
* pour chaque espace HumHub dont il fait partie, il est ajouté au groupe correspondant sur le yeswiki site vitrine nommé «
  *nom-espace* » (
  *humhub-nom-espace* l'URL de l’espace d’apprentissage HumHub) et s’il est administrateur de ce groupe il est également au groupe «
  *humhub-nom-espace-admins* »

Pour chaque rôle, une synchronisation est effectuée de la même façon si la table correspondante existe dans la base de données. Mais pour garantir qu’un utilisateur ait le même identifiant dans tous les yeswiki synchronisés, on ne recherche pas uniquement la disponibilité de cet identifiant au sein du wiki associé à son rôle, mais dans l’ensemble des wikis associés à tous ses rôles.\
Par exemple, si un utilisateur comporte les rôles « Jardinier » et « Apprenant·e », on affectera son identifiant en vérifiant dans les deux wikis associés à ces rôles que cet identifiant n’existe pas déjà en tant que nom d’utilisateur ou en tant que nom de page.

Concernant les droits yeswiki des fiches des profils utilisateurs, ils sont définis comme suit :

* en lecture, puisqu'on synchronise un wiki par rapport à un rôle et qu’un rôle comporte un espace par défaut, les fiches des profils pourront être consultées seulement par les membres de cet espace par défaut. Par exemple, si un utilisateur a le rôle « Jardinier·e » et « Apprenant·e Orange » et que chacun de ces rôles est associé à un wiki à synchroniser (c'est-à-dire qu’il existe les tables associées dans la base de données, cf plus haut), les droits en lecture du profil prendront la valeur de chacun des espaces par défaut de ces rôles, soit pour le premier wiki : « humhub-salle-commune » et « humhub-orange » pour le deuxième wiki.
* en écriture, « @admins » qui permet de laisser le droit de modification du profil qu’aux administrateurs du yeswiki en question. Les utilisateurs ne peuvent ainsi pas modifier leurs informations de profil sur YesWiki et doivent passer par HumHub pour le faire (la synchronisation se fait uniquement dans le sens HumHub -> YesWiki).

Une dernière particularité concerne les rôles qui commencent par « Apprenant·e », pour ceux-ci on renseigne dans la page de leur profil une version minimale du profil utilisateur (un sous-ensemble des champs de profil d’un Jardinier).

## Exigences & Compatibilité

- Le [module Name Parser](https://marketplace.humhub.com/module/name-parser/description) doit être installé et dans la configuration du module, ne pas cocher "Reformater automatiquement le nom d'utilisateur à partir du prénom et du nom" car ce module reformate déjà le nom d'utilisateur
- Le champ de profil "Date de naissance" (`birthday`) doit être obligatoire
- Ce module est compatible avec les modules Classified Space et Category Group pour afficher la liste des espaces des catégories relatives aux groupes dans l'admin

### YesWiki

Les tables YesWiki doivent exister dans la BDD (voir répertoire `models/yeswiki`).

Dans `protected/admin/common.php`, rajouter dans `'components'`
```
        'dbYesWiki' => [
          'class' => 'yii\\db\\Connection',
          'dsn' => 'mysql:host=localhost;dbname=xxxxx',
          'username' => 'xxxxx',
          'password' => ''xxxxx,
          'charset' => 'utf8mb4',
        ],
```

Dans la configuration Apache du domaine de YesWiki, rajouter ce code pour gérer la déconnexion de YesWiki à partir de HumHub :
```
Header always set Access-Control-Allow-Credentials "true"
Header always set Access-Control-Allow-Origin "https://grandjardin.jardiniersdunous.org"
Header always set Access-Control-Allow-Headers "Authorization"
Header always set Access-Control-Allow-Methods "GET"
Header always set Access-Control-Expose-Headers "Content-Security-Policy, Location"
Header always set Access-Control-Max-Age "600"
```

Dans YesWiki, créer un formulaire bazar dont l’« id_typeannonce » est 1000 (paramétrable). Chaque profil sera alors enregistré sur une fiche de YesWiki (page particulière qui suit la structure du formulaire) et porte le nom de l’identifiant donné à l’utilisateur.

Champs obligatoires :
- bf_email
- bf_titre
- bf_url_photo

Champs optionnels à paramétrer dans protected/config/common.php : voir les attributs possibles dans la classe `Module`

### Champs de profil sur HumHub

Ce module gère ces champs de profil (optionnel) :
- `tranche_age` (type "liste de choix") - "Tranche d’âge" :
  1=>0-19 ans 2=>20-29 ans 3=>30-39 ans 4=>40-49 ans 5=>50-59 ans 6=>60 ans et +
- `domaines_interets` (type "texte") - "Domaines d'intérêts (mots clés séparés par des virgules)"

Le reste, c'est dans protected/config/common.php (voir les attributs de humhub-modules-yeswiki/Module.php)

## Opérations de maintenance

Commandes possibles : voir la documentation des méthodes de classe YeswikiController

## Repository

https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki

## Licence

[GNU AGPL 3](https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki/blob/main/docs/LICENCE.md) 
