<?php
/**
 * YesWiki
 * @link https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki
 * @license https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki/blob/main/docs/LICENCE.md
 * @author [Marc Farré](https://marc.fun)
 */

use humhub\commands\CronController;
use humhub\components\console\Application;
use humhub\modules\admin\widgets\AdminMenu;
use humhub\modules\space\models\Membership;
use humhub\modules\space\models\Space;
use humhub\modules\space\widgets\HeaderControlsMenu;
use humhub\modules\user\models\forms\Registration;
use humhub\modules\user\models\GroupUser;
use humhub\modules\user\models\Profile;
use humhub\modules\user\models\User;
use humhub\modules\yeswiki\Events;

/** @noinspection MissedFieldInspection */
return [
    'id' => 'yeswiki',
    'class' => 'humhub\modules\yeswiki\Module',
    'namespace' => 'humhub\modules\yeswiki',
    'events' => [
        [
            'class' => Application::class,
            'event' => Application::EVENT_ON_INIT,
            'callback' => [Events::class, 'onConsoleApplicationInit']
        ],
        [
            'class' => AdminMenu::class,
            'event' => AdminMenu::EVENT_INIT,
            'callback' => [Events::class, 'onAdminMenuInit']
        ],
        [
            'class' => CronController::class,
            'event' => CronController::EVENT_ON_DAILY_RUN,
            'callback' => [Events::class, 'onCronDailyRun']
        ],

        [
            // When a new user register, EVENT_AFTER_INSERT is executed just after the user has successfully validated the registration form (before \humhub\modules\user\models\forms\Registration::EVENT_AFTER_REGISTRATION)
            'class' => Profile::class,
            'event' => Profile::EVENT_AFTER_INSERT,
            'callback' => [Events::class, 'onModelProfileAfterInsert']
        ],
        [
            // EVENT_AFTER_UPDATE is executed each time profile is updated or on user delete
            'class' => Profile::class,
            'event' => Profile::EVENT_AFTER_UPDATE,
            'callback' => [Events::class, 'onModelProfileAfterUpdate']
        ],
        [
            'class' => User::class,
            'event' => User::EVENT_BEFORE_SOFT_DELETE,
            'callback' => [Events::class, 'onModelUserBeforeDelete']
        ],
        [
            'class' => User::class,
            'event' => User::EVENT_AFTER_UPDATE,
            'callback' => [Events::class, 'onModelUserAfterUpdate']
        ],
//        [
//            // Caution : EVENT_AFTER_LOGIN is not executed if user creates a new account. In this cas, use \humhub\modules\user\models\forms\Registration::EVENT_AFTER_REGISTRATION
//            'class' => \yii\web\User::class,
//            'event' => \yii\web\User::EVENT_AFTER_LOGIN,
//            'callback' => [
//                Events::class,
//                'onAfterLogin'
//            ]
//        ],
        [
            // When a new user register, EVENT_AFTER_REGISTRATION is executed just after \humhub\modules\user\models\Profile::EVENT_AFTER_INSERT (after the user has successfully validated the registration form)
            'class' => Registration::class,
            'event' => Registration::EVENT_AFTER_REGISTRATION,
            'callback' => [Events::class, 'onFormAfterRegistration']
        ],

        // *** GROUP ***
        [
            'class' => GroupUser::class,
            'event' => GroupUser::EVENT_AFTER_INSERT,
            'callback' => [Events::class, 'onModelGroupUserAfterInsert']
        ],
        [
            'class' => GroupUser::class,
            'event' => GroupUser::EVENT_AFTER_DELETE,
            'callback' => [Events::class, 'onModelGroupUserAfterDelete']
        ],

        // *** SPACES ***

        // Synchronisation des espaces HumHub avec les groupes YesWiki
        // Afin de pouvoir donner des droits particuliers aux pages YesWiki en fonction des espaces auxquels adhère un utilisateur, la liste des membres des espaces sous HumHub sont synchronisés dans des groupes YesWiki. Encore une fois, cette synchronisation se fait uniquement dans le sens HumHub -> YesWiki lors d’un ajout ou la suppression d’un utilisateur.
        // Pour chaque espace HumHub, deux groupes YesWiki sont créés et mis à jour :
        // -   le premier réunit l’ensemble des membres de l’espace et le groupe prend comme nom un identifiant unique créé à partir du nom de l’espace. Par exemple, les utilisateurs de l’espace « Outils numériques » sur HumHub seront réunis dans le groupe « OutilsNumeriques » dans YesWiki.
        // -   le deuxième groupe se restreint uniquement aux administrateurs de l’espace. Celui-ci comporte le même nom que le groupe ci-dessus mais avec « Admins » à la fin. Par exemple, l’espace « Cercle Général JdN » deviendra le groupe « CercleGeneralJdnAdmins » dans YesWiki.
        // A chaque fois qu’un espace est ajouté ou supprimé, cela mettra à jour la liste des groupes sur YesWiki. Mais également si un espace change de nom, le nom de son groupe sera mise à jour et ce groupe gardera l’ensemble de ses droits dans YesWiki.
        [
            'class' => Space::class,
            'event' => Space::EVENT_AFTER_INSERT,
            'callback' => [Events::class, 'onModelSpaceAfterInsert']
        ],
        [
            // use _BEFORE_ and not _AFTER_
            'class' => Space::class,
            'event' => Space::EVENT_BEFORE_DELETE,
            'callback' => [Events::class, 'onModelSpaceBeforeDelete']
        ],
        [   // use _BEFORE_ and not _AFTER_
            'class' => Space::class,
            'event' => Space::EVENT_BEFORE_UPDATE,
            'callback' => [Events::class, 'onModelSpaceBeforeUpdate']
        ],
        [
            'class' => Membership::class,
            'event' => Membership::EVENT_MEMBER_ADDED,
            'callback' => [Events::class, 'onModelSpaceMembershipMemberAdded']
        ],
        [
            'class' => Membership::class,
            'event' => Membership::EVENT_MEMBER_REMOVED,
            'callback' => [Events::class, 'onModelSpaceMembershipMemberRemoved']
        ],
        [
            'class' => Membership::class,
            'event' => Membership::EVENT_AFTER_UPDATE,
            'callback' => [Events::class, 'onModelSpaceMembershipAfterUpdate']
        ],

        // *** WIDGETS ***
        [
            'class' => HeaderControlsMenu::class,
            'event' => HeaderControlsMenu::EVENT_INIT,
            'callback' => [Events::class, 'onSpaceAdminMenuInit']
        ],
    ]
];
?>
