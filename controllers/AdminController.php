<?php
/**
 * YesWiki
 * @link https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki
 * @license https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki/blob/main/docs/LICENCE.md
 * @author [Marc Farré](https://marc.fun)
 */

namespace humhub\modules\yeswiki\controllers;

use humhub\modules\admin\components\Controller;
use humhub\modules\user\models\User;
use humhub\modules\yeswiki\jobs\Sync;
use humhub\modules\yeswiki\models\GroupSearch;
use humhub\modules\yeswiki\models\UserNotExtended;
use humhub\modules\yeswiki\models\Yeswiki;
use humhub\widgets\ModalClose;
use Yii;
use yii\helpers\Html;

class AdminController extends Controller
{
    /**
     * @inheritdoc
     */
    public $adminOnly = false;

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new GroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionEditTablePrefix($groupId)
    {
        $model = Yeswiki::findOne($groupId) ?? new Yeswiki(['group_id' => $groupId]);

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
            return ModalClose::widget(['saved' => true, 'reload' => true]);
        }

        return $this->renderAjax('editTablePrefix',
            [
                'model' => $model,
            ]
        );
    }

    public function actionSync()
    {
        Yii::$app->queue->push(new Sync());
        $this->view->success('La synchronisation a démarré');
        $this->redirect('index');
    }

    /**
     * Update profile, parse username and update username in all sites
     * Usage: /yeswiki/admin/parse-user?id=xxx
     * @param $id
     * @return string
     * @throws \Throwable
     * @throws \yii\base\NotSupportedException
     * @throws \yii\db\StaleObjectException
     */
    public function actionParseUser($id)
    {
        $userNotExtended = UserNotExtended::findOne($id);
        if ($userNotExtended === null || $userNotExtended->status !== User::STATUS_ENABLED) {
            return 'User not found or not enabled';
        }

        // Update HumHub profile
        $userNotExtended->updateProfile();

        // Update username from first and last name, checking if username is available in all YesWiki sites
        $userNotExtended->checkUsernameAvailabilityAndUpdateAllSites();

        return 'User ' . Html::encode($userNotExtended->username) . ' parsed successfully and all sites updated';
    }
}
