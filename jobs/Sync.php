<?php
/**
 * YesWiki
 * @link https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki
 * @license https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki/blob/main/docs/LICENCE.md
 * @author [Marc Farré](https://marc.fun)
 */

namespace humhub\modules\yeswiki\jobs;


use humhub\modules\queue\ActiveJob;
use humhub\modules\queue\interfaces\ExclusiveJobInterface;
use humhub\modules\space\models\Space;
use humhub\modules\user\models\Group;
use humhub\modules\user\models\User;
use humhub\modules\yeswiki\models\UserNotExtended;
use humhub\modules\yeswiki\models\yeswiki\YesWikiTriples;
use humhub\modules\yeswiki\models\yeswiki\YesWikiUsers;
use ReflectionClass;
use Yii;
use yii\queue\RetryableJobInterface;

class Sync extends ActiveJob implements ExclusiveJobInterface, RetryableJobInterface
{
    /**
     * @inhertidoc
     * @var int maximum 1 hour
     */
    private $maxExecutionTime = 60 * 60;

    /**
     * @inheritdoc
     */
    public function run()
    {
        // Delete all users and groups
        $groupsQuery = Group::find()
            ->leftJoin('yeswiki', 'group.id=yeswiki.group_id')
            ->andWhere(['not', ['yeswiki.table_prefix' => [null, '']]]);
        foreach ($groupsQuery->each() as $group) {
            YesWikiUsers::deleteAllUsers($group);
            YesWikiTriples::deleteAllGroups($group);
        }

        $usersNotExtended = UserNotExtended::find()
            ->where(['status' => User::STATUS_ENABLED]);

        /** @var UserNotExtended $userNotExtended */
        foreach ($usersNotExtended->each(500) as $userNotExtended) {
            // Check if username is not already taken because of a similar tag in YesWiki
            $userNotExtended->checkUsernameAvailabilityAndUpdateAllSites();

            // Add YesWiki user if not exists
            YesWikiUsers::addUserAndProfileInAllSites($userNotExtended);

            // Add user in YesWiki's groups
            /** @var Group $group */
            foreach ($userNotExtended->syncGroups as $group) {
                /** @var Space $space */
                foreach ($userNotExtended->spaces as $space) {
                    // Add user in YesWiki group (and create group if doesn't exist)
                    YesWikiTriples::addMemberInGroup($group, $space, $userNotExtended->username);

                    // If is admin
                    $membership = $space->getMembership($userNotExtended->id);
                    if ($membership->group_id === Space::USERGROUP_ADMIN) {
                        // Add as admin
                        YesWikiTriples::addMemberInGroup($group, $space, $userNotExtended->username, true);
                    }
                }
            }
        }
        Yii::warning('La synchronisation YesWiki est terminée', 'yeswiki');
    }

    /**
     * @inheritDoc
     */
    public function getTtr()
    {
        return $this->maxExecutionTime;
    }

    /**
     * @inheritDoc
     */
    public function canRetry($attempt, $error)
    {
        $errorMessage = $error ? $error->getMessage() : '';
        Yii::error('Error with Sync job: ' . $errorMessage, 'yeswiki');
        return false;
    }

    /**
     * @inerhitdoc
     * Must not exceed to 50 chars
     */
    public function getExclusiveJobId()
    {
        return 'yeswiki.' . (new ReflectionClass($this))->getShortName();
    }
}
