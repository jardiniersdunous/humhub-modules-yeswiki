<?php
/**
 * YesWiki
 * @link https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki
 * @license https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki/blob/main/docs/LICENCE.md
 * @author [Marc Farré](https://marc.fun)
 */

use humhub\modules\categoryGroup\models\CategoryGroup;
use humhub\modules\classifiedSpace\models\ClassifiedSpaceCategory;
use humhub\modules\space\models\Space;
use humhub\modules\ui\view\components\View;
use humhub\modules\user\models\Group;
use humhub\modules\yeswiki\models\GroupSearch;
use humhub\modules\yeswiki\models\Yeswiki;
use humhub\modules\yeswiki\models\yeswiki\YesWikiTriples;
use humhub\widgets\Button;
use humhub\widgets\GridView;
use humhub\widgets\Label;
use humhub\widgets\ModalButton;
use yii\data\ActiveDataProvider;

/* @var $this View */
/* @var $dataProvider ActiveDataProvider */
/* @var $searchModel GroupSearch */


/** @var \humhub\modules\categoryGroup\Module $categoryGroupModule */
$categoryGroupModule = Yii::$app->getModule('category-group');

$columns = [
    'id',
    [
        'attribute' => 'name',
        'format' => 'raw',
        'value' => static function (Group $group) {
            return Button::info($group->name)->link(['/admin/group/edit', 'id' => $group->id])->sm()->style(['white-space' => 'normal']);
        }
    ],
    [
        'header' => 'Préfixe table YesWiki',
        'format' => 'raw',
        'value' => static function (Group $group) {
            $yeswiki = Yeswiki::findOne(['group_id' => $group->id]);
            return
                ($yeswiki->table_prefix ?? '') . ' ' .
                ModalButton::primary()->load(['/yeswiki/admin/edit-table-prefix', 'groupId' => $group->id])->icon('pencil')->sm();
        }
    ],
    [
        'header' => 'Synchro YesWiki',
        'format' => 'raw',
        'value' => static function (Group $group) {
            $yeswiki = Yeswiki::findOne(['group_id' => $group->id]);
            $labelType = Label::TYPE_DEFAULT;
            $message = 'Pas de synchro';
            $tooltip = 'Le nom de la table n\'est pas renseigné';
            if ($yeswiki && $yeswiki->table_prefix) {
                $labelType = Label::TYPE_DANGER;
                $message = 'Tables non trouvées';
                $tooltip = 'La synchronisation ne peut pas se faire, vérifier les tables YesWiki';
                if ($yeswiki->tablesExists()) {
                    $labelType = Label::TYPE_SUCCESS;
                    $message = 'Synchro activée !';
                    $tooltip = 'Les tables sont bien trouvées !';
                }
            }

            return Label::instance($message)->setType($labelType)->tooltip($tooltip);
        }
    ],
    [
        'attribute' => 'members',
        'label' => Yii::t('AdminModule.user', 'Members'),
        'format' => 'raw',
        'options' => ['style' => 'text-align:center;'],
        'value' => function ($data) {
            return $data->getGroupUsers()->count();
        }
    ],
];

if ($categoryGroupModule) {
    $columns[] = [
        'header' => 'Espaces liés',
        'format' => 'raw',
        'value' => static function (Group $group) {
            $categoryGroup = CategoryGroup::findOne(['group_id' => $group->id]);
            if (!$categoryGroup) {
                return '';
            }

            $category = $categoryGroup->category;
            if (!$categoryGroup->category instanceof ClassifiedSpaceCategory) {
                return '';
            }

            $spaceValues = [];
            /** @var Space $space */
            foreach ($category->getSpaceQuery()->all() as $space) {
                $spaceValues[] =
                    Button::info($space->name)
                        ->link($space->getUrl())
                        ->sm() . '<br>' .
                    Label::defaultType('@' . YesWikiTriples::groupNameFromHumhubSpace($space))
                        ->tooltip('Ce nom est celui du groupe YesWiki correspondant à cet espace. Tous les membres de cet espace sont dans ce groupe du YesWiki correspondant à leur(s) rôle(s).')
                        ->style(['margin-left' => '8px; text-transform: none;']) . '<br>' .
                    Label::defaultType('@' . YesWikiTriples::groupNameFromHumhubSpace($space, true))
                        ->tooltip('Ce nom est celui du groupe YesWiki correspondant à cet espace. Tous les administrateurs de cet espace sont dans ce groupe du YesWiki correspondant à leur(s) rôle(s).')
                        ->style(['margin-left' => '8px; text-transform: none;']);
            }
            return implode('<br>', $spaceValues);
        }
    ];
}
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <?= Button::back(['index'])->right() ?>
        <strong>Synchro YesWiki avec les groupes</strong>
    </div>
    <div class="panel-body">
        <div class="panel-body panel-danger">
            <strong>IMPORTANT ! Procédure pour créer un groupe dont les utilisateurs et espaces sont synchronisés avec
                YesWiki
                :</strong>
            <ol>
                <li>S'il n'existe pas, créer l'espace par défaut du groupe.</li>
                <li>Ajouter le groupe. Pour les groupes "Apprenant·e", le nom doit être <code>Apprenant·e Nom du futur
                        espace
                        par défaut</code>.
                </li>
                <li>Indiquer l'espace par défaut (attention, ce nouveau groupe ne sera pas ajouté automatiquement aux
                    membres de
                    cet espace).
                </li>
                <li>Créer les tables du site YesWiki correspondant en utilisant le préfixe indiqué sur cette page.</li>
                <li>Vérifier qu'elles sont bien détectées pour le groupe en revenant sur cette page.</li>
            </ol>
            <p><strong>Pour les groupes liés à un site YesWiki</strong> ne jamais changer le nom, même si le nom de
                l'espace a
                changé, car sinon il faudrait changer le nom des tables YesWiki correspondantes !</p>
            <p><i>La création des tables n'est pas obligatoire, mais il n'y aura pas de synchronisation des
                    utilisateurs, des
                    profils et des espaces avec les groupes YesWiki.</i></p>
            <p><i>La synchronisation avec YesWiki ne se fait que lorsque l'on change les groupes à un utilisateur ou
                    s'il
                    devient membre ou quitte un espace, mais pas à la création d'un groupe ni d'un espace.</i></p>
            <p>Plus d'infos dans docs/README.md</p>
        </div>

        <hr>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table table-hover'],
            'columns' => $columns,
        ]) ?>

        <hr>
        <?= Button::danger('Synchroniser à nouveau tous les YesWikis')->link(['sync'])->confirm() ?>
    </div>
</div>
