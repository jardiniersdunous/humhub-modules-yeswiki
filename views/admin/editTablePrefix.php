<?php

use humhub\modules\ui\form\widgets\ActiveForm;
use humhub\modules\ui\view\components\View;
use humhub\modules\yeswiki\models\Yeswiki;
use humhub\widgets\ModalButton;
use humhub\widgets\ModalDialog;

/* @var $this View */
/* @var $model Yeswiki|null */
?>

<?php ModalDialog::begin([
    'header' => 'Edition du préfixe de la table YesWiki',
]) ?>
<div class="modal-body">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'table_prefix')->textInput() ?>
    <?= ModalButton::submitModal()->confirm() ?>
    <?php ActiveForm::end(); ?>
</div>
<div class="modal-footer" style="text-align: center;">
    <?= ModalButton::cancel(Yii::t('base', 'Close')) ?>
</div>
<?php ModalDialog::end() ?>
