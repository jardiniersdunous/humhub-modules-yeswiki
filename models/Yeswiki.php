<?php
/**
 * YesWiki
 * @link https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki
 * @license https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki/blob/main/docs/LICENCE.md
 * @author [Marc Farré](https://marc.fun)
 */

namespace humhub\modules\yeswiki\models;

use humhub\components\ActiveRecord;
use humhub\modules\user\models\Group;
use humhub\modules\yeswiki\models\yeswiki\YesWikiUsers;
use Yii;
use yii\behaviors\AttributeTypecastBehavior;
use yii\db\ActiveQuery;

/**
 * @property int $group_id
 * @property string $table_prefix
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 *
 * @property Group $group
 */
class Yeswiki extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yeswiki';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'group_id' => 'ID du groupe',
            'table_prefix' => 'Préfixe de la table',
            'created_at' => Yii::t('UserModule.base', 'Created at'),
            'created_by' => Yii::t('UserModule.base', 'Created by'),
            'updated_at' => Yii::t('UserModule.base', 'Updated at'),
            'updated_by' => Yii::t('UserModule.base', 'Updated by'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // See https://www.yiiframework.com/doc/guide/2.0/en/tutorial-core-validators
            [['group_id'], 'required'],
            [['group_id'], 'integer'],
            [['table_prefix'], 'unique'],
            [['table_prefix'], 'string', 'max' => 127],
        ];
    }

    /**
     * Compose automatically 'attributeTypes' according to `rules()` to maintain strict attribute types after model validation.
     * Avoid having changed attributes in `afterSave()` $changedAttributes if they are not string
     * https://www.yiiframework.com/doc/api/2.0/yii-behaviors-attributetypecastbehavior
     * If saving from a form and the value can be null, you need to typecast '' value to null in a beforeValidate() method
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'typecast' => [
                'class' => AttributeTypecastBehavior::class,
            ],
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::class, ['id' => 'group_id']);
    }

    /**
     * Check if YesWiki's tables corresponding to $humhubGroup are created in MySQL
     * @return bool
     */
    public function tablesExists()
    {
        // We assume that if users table exists, all other tables for YesWiki exists also
        return (new YesWikiUsers(['humhubGroupId' => $this->group_id]))->_tableExists;
    }
}