<?php
/**
 * YesWiki
 * @link https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki
 * @license https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki/blob/main/docs/LICENCE.md
 * @author [Marc Farré](https://marc.fun)
 */

namespace humhub\modules\yeswiki\models;

use DateTime;
use humhub\modules\nameParser\services\NameParserService;
use humhub\modules\space\models\Space;
use humhub\modules\user\models\Group;
use humhub\modules\user\models\GroupUser;
use humhub\modules\user\models\User;
use humhub\modules\yeswiki\models\yeswiki\YesWikiUsers;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;


/**
 * IMPORTANT !!!! Don't `extends \humhub\modules\user\models\User` as it will generate an event and execute onModelProfileUpdate again !
 *
 * @property integer $id
 * @property string $guid
 * @property integer $status
 * @property string $username
 * @property string $email
 * @property string $auth_mode
 * @property string $language
 * @property string $time_zone
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $last_login
 * @property string $authclient_id
 * @property string $auth_key
 * @property integer $visibility
 * @property integer $contentcontainer_id
 *
 * @property ProfileNotExtended $profile
 * @property GroupUser[] $groupUsers
 * @property Group[] $groups
 * @property Group[] $syncGroups
 * @property Space[] $spaces
 */
class UserNotExtended extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    public function getProfile()
    {
        return $this->hasOne(ProfileNotExtended::class, ['user_id' => 'id']);
    }

    /**
     * Returns all Group relations of this user as ActiveQuery
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Group::class, ['id' => 'group_id'])->via('groupUsers');
    }

    /**
     * Returns all Group relations of this user as ActiveQuery that are synced with YesWiki
     * @return \yii\db\ActiveQuery
     */
    public function getSyncGroups()
    {
        return $this->hasMany(Group::class, ['id' => 'group_id'])->via('groupUsers')
            ->leftJoin('yeswiki', 'group.id=yeswiki.group_id')
            ->andWhere(['not', ['yeswiki.table_prefix' => [null, '']]]);
    }

    /**
     * Returns all GroupUser relations of this user as ActiveQuery
     * @return \yii\db\ActiveQuery
     */
    public function getGroupUsers()
    {
        return $this->hasMany(GroupUser::class, ['user_id' => 'id']);
    }

    /**
     *
     * @return ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getSpaces()
    {
        // TODO: SHOW ONLY REAL MEMBERSHIPS
        return $this->hasMany(Space::class, ['id' => 'space_id'])
            ->viaTable('space_membership', ['user_id' => 'id']);
    }

    /**
     * Search if username is available (in HumHub users and tag names in all YesWiki corresponding to the user's roles)
     * and update username in Huhub and  all YesWiki sites
     * @return bool
     * @throws \Throwable
     * @throws \yii\base\NotSupportedException
     * @throws \yii\db\StaleObjectException
     */
    public function checkUsernameAvailabilityAndUpdateAllSites()
    {
        // profile->firstname can be empty if new account creation
        if (empty($this->profile->firstname) || empty($this->profile->lastname)) {
            return false;
        }

        // Parse username
        $newUsername = NameParserService::parseUsername(User::findOne($this->id));

        // If username already exists in other YesWikis, add a number at the end
        $newUsername = $this->addNumberToUsernameIfExists($newUsername);

        if (!$newUsername || $this->username === $newUsername) {
            return false;
        }

        // Save new username
        $this->username = $newUsername;
        $this->save();

        // If this is current user and not just after registration, set new username for current session
        $currentUser = Yii::$app->user->identity;
        if (
            $currentUser !== null // if new registration
            && $this->id === $currentUser->id
        ) {
            // Save new username
            $currentUser->username = $newUsername;
            $currentUser->save();
        }

        // Update YesWiki username (based on email correspondance)
        // And add user if not exists
        YesWikiUsers::updateUsernameInAllSites($this);

        return true;
    }

    /**
     * @param $username
     * @param $number
     * @return string
     * @throws \yii\base\NotSupportedException
     */
    private function addNumberToUsernameIfExists($username, $number = 1)
    {
        // We try max 100 times
        if ($number > 100) {
            return false;
        }

        $newUsername = $number === 1 ? $username : $username . $number;

        return (
            NameParserService::usernameIsValid($newUsername, $this->id)
            && YesWikiUsers::usernameIsAvailableInAllSites($this, $newUsername)
        ) ?
            $newUsername :
            $this->addNumberToUsernameIfExists($username, $number + 1);
    }


    /**
     * @return void
     * @throws \Exception
     */
    public function updateProfile(bool $ageRangeOnly = false)
    {
        $user = User::findOne($this->id);
        if (!$user->isActive()) {
            return;
        }

        $profile = $this->profile; // To avoid looping in Events
        $saveNewProfile = false;

        if ($profile->birthday && $profile->hasAttribute('tranche_age')) {
            // Update tranche d'âge
            $age = (new DateTime())->diff(new DateTime($profile->birthday))->y;
            if ($age < 20) {
                $newAgeRange = 1;
            } elseif ($age < 30) {
                $newAgeRange = 2;
            } elseif ($age < 40) {
                $newAgeRange = 3;
            } elseif ($age < 50) {
                $newAgeRange = 4;
            } elseif ($age < 60) {
                $newAgeRange = 5;
            } else {
                $newAgeRange = 6;
            }
            if ($profile->tranche_age !== $newAgeRange) {
                $profile->tranche_age = $newAgeRange;
                $saveNewProfile = true;
            }
        }

        if (!$ageRangeOnly) {
            // Remove spaces in the zip (otherwise user will not be indexed in the search engine)
            $newZip = str_ireplace(' ', '', (string)$profile->zip);
            if ($profile->zip !== $newZip) {
                $profile->zip = $newZip;
                $saveNewProfile = true;
            }
        }

        if ($saveNewProfile && !$profile->save()) {
            Yii::error('Error saving profile while updating profile for user ' . $user->displayName . ' (ID ' . $this->id . '): ' . implode(' ', $profile->getErrorSummary(true)), 'yeswiki');
        }

        if ($ageRangeOnly) {
            return;
        }

        // Duplicate profile field `domaines_interets` in user table
        if ($profile->hasAttribute('domaines_interets')) {
            $tags = [];
            foreach (explode(',', (string)$profile->domaines_interets) as $tag) {
                $tags[] = trim((string)$tag);
            }
            $user->tagsField = $tags;
            if (!$user->save()) {
                Yii::error('Error saving user while updating profile ' . $user->displayName . ' (ID ' . $this->id . '): ' . implode(' ', $profile->getErrorSummary(true)), 'yeswiki');
            }
        }
    }
}
