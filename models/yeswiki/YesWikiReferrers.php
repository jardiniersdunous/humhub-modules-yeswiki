<?php
/**
 * YesWiki
 * @link https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki
 * @license https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki/blob/main/docs/LICENCE.md
 * @author [Marc Farré](https://marc.fun)
 */

namespace humhub\modules\yeswiki\models\yeswiki;

use humhub\components\ActiveRecord;
use humhub\modules\user\models\Group;
use humhub\modules\yeswiki\models\Yeswiki;
use Yii;
use yii\base\NotSupportedException;


/**
 * @property string $page_tag
 * @property string $referrer
 * @property string $time
 */
class YesWikiReferrers extends ActiveRecord
{
    const TABLE_NAME_SUFFIX = 'referrers';

    /**
     * @var string the associated database table name
     */
    public static $tableName;

    /**
     * @var int HumHub Group ID
     */
    public $humhubGroupId;

    /**
     * @var bool
     */
    public $_tableExists = false;

    /**
     * @inerhitdoc
     */
    public function __construct($config = [])
    {
        // Reinitialize because value is the same as the previous instance
        $this->_tableExists = false;

        // Get table name
        $yeswiki = Yeswiki::findOne($config['humhubGroupId'] ?? $this->humhubGroupId);
        if ($yeswiki === null || !$yeswiki->table_prefix) {
            return;
        }
        static::$tableName = $yeswiki->table_prefix . self::TABLE_NAME_SUFFIX;

        // Check if table exists
        try {
            $tableSchema = static::getDb()
                ->getSchema()
                ->getTableSchema(static::tableName());
        } catch (NotSupportedException $e) {
            return;
        }
        $this->_tableExists = $tableSchema !== null;

        if ($this->_tableExists) {
            parent::__construct($config);
        }
    }

    public static function getDb()
    {
        return Yii::$app->dbYesWiki;
    }

    public static function tableName()
    {
        return static::$tableName;
    }

    /**
     * @param Group $humhubGroup
     * @param $oldPageTag
     * @param $newPageTag
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public static function updateTag($humhubGroup, $oldPageTag, $newPageTag)
    {
        $yesWikiReferrersModel = new self(['humhubGroupId' => $humhubGroup->id]);
        if (!$yesWikiReferrersModel->_tableExists) {
            return false;
        }

        // If tags already exist with the new name, delete them
        $yesWikiReferrers = $yesWikiReferrersModel::findAll(['page_tag' => $newPageTag]);
        foreach ($yesWikiReferrers as $yesWikiReferrer) {
            $yesWikiReferrer->delete();
        }

        $yesWikiReferrers = $yesWikiReferrersModel::findAll(['page_tag' => $oldPageTag]);
        foreach ($yesWikiReferrers as $yesWikiReferrer) {
            $yesWikiReferrer->page_tag = $newPageTag;
            $yesWikiReferrer->save();
        }

        return true;
    }

    /**
     * @param Group $humhubGroup
     * @param $pageTag
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public static function deleteTag($humhubGroup, $pageTag)
    {
        $yesWikiReferrersModel = new self(['humhubGroupId' => $humhubGroup->id]);
        if (!$yesWikiReferrersModel->_tableExists) {
            return false;
        }

        $yesWikiReferrers = $yesWikiReferrersModel::findAll(['page_tag' => $pageTag]);
        foreach ($yesWikiReferrers as $yesWikiReferrer) {
            $yesWikiReferrer->delete();
        }

        return true;
    }


    /*
        public function attributeLabels()
        {
            return [
            ];
        }

        public function rules()
        {
           return [
           ];
        }
    */
}

?>
