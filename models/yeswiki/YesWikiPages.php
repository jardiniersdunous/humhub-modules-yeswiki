<?php
/**
 * YesWiki
 * @link https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki
 * @license https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki/blob/main/docs/LICENCE.md
 * @author [Marc Farré](https://marc.fun)
 */

namespace humhub\modules\yeswiki\models\yeswiki;

use humhub\components\ActiveRecord;
use humhub\modules\membersMap\models\GeocodingResult;
use humhub\modules\membersMap\services\Geocode;
use humhub\modules\user\models\Group;
use humhub\modules\user\models\Profile;
use humhub\modules\user\models\User;
use humhub\modules\yeswiki\models\UserNotExtended;
use humhub\modules\yeswiki\models\Yeswiki;
use humhub\modules\yeswiki\Module;
use Yii;
use yii\base\NotSupportedException;
use yii\helpers\Url;

/**
 * @property int $id
 * @property string $tag
 * @property string $time
 * @property string $body
 * @property string $body_r
 * @property string $owner
 * @property string $user
 * @property string $latest
 * @property string $handler
 * @property string $comment_on
 */
class YesWikiPages extends ActiveRecord
{
    const TABLE_NAME_SUFFIX = 'pages';

    const PROFILE_VAL_HUMHUB_2_YESWIKI = [
        'country' => [
            'AF' => 'Afghanistan',
            'ZA' => 'Afrique Du Sud',
            'AX' => 'Åland, Îles',
            'AL' => 'Albanie',
            'DZ' => 'Algérie',
            'DE' => 'Allemagne',
            'AD' => 'Andorre',
            'AO' => 'Angola',
            'AI' => 'Anguilla',
            'AQ' => 'Antarctique',
            'AG' => 'Antigua-Et-Barbuda',
            'SA' => 'Arabie Saoudite',
            'AR' => 'Argentine',
            'AM' => 'Arménie',
            'AW' => 'Aruba',
            'AU' => 'Australie',
            'AT' => 'Autriche',
            'AZ' => 'Azerbaïdjan',
            'BS' => 'Bahamas',
            'BH' => 'Bahreïn',
            'BD' => 'Bangladesh',
            'BB' => 'Barbade',
            'BY' => 'Bélarus',
            'BE' => 'Belgique',
            'BZ' => 'Belize',
            'BJ' => 'Bénin',
            'BM' => 'Bermudes',
            'BT' => 'Bhoutan',
            'BO' => 'Bolivie, L\'état Plurinational De',
            'BQ' => 'Bonaire, Saint-Eustache Et Saba',
            'BA' => 'Bosnie-Herzégovine',
            'BW' => 'Botswana',
            'BV' => 'Bouvet, Île',
            'BR' => 'Brésil',
            'BN' => 'Brunei Darussalam',
            'BG' => 'Bulgarie',
            'BF' => 'Burkina Faso',
            'BI' => 'Burundi',
            'KY' => 'Caïmans, Îles',
            'KH' => 'Cambodge',
            'CM' => 'Cameroun',
            'CA' => 'Canada',
            'CV' => 'Cap-Vert',
            'CF' => 'Centrafricaine, République',
            'CL' => 'Chili',
            'CN' => 'Chine',
            'CX' => 'Christmas, Île',
            'CY' => 'Chypre',
            'CC' => 'Cocos (Keeling), Îles',
            'CO' => 'Colombie',
            'KM' => 'Comores',
            'CG' => 'Congo',
            'CD' => 'Congo, La République Démocratique Du',
            'CK' => 'Cook, Îles',
            'KR' => 'Corée, République De',
            'KP' => 'Corée, République Populaire Démocratique De',
            'CR' => 'Costa Rica',
            'CI' => 'Côte D\'ivoire',
            'HR' => 'Croatie',
            'CU' => 'Cuba',
            'CW' => 'Curaçao',
            'DK' => 'Danemark',
            'DJ' => 'Djibouti',
            'DO' => 'Dominicaine, République',
            'DM' => 'Dominique',
            'EG' => 'Égypte',
            'SV' => 'El Salvador',
            'AE' => 'Émirats Arabes Unis',
            'EC' => 'Équateur',
            'ER' => 'Érythrée',
            'ES' => 'Espagne',
            'EE' => 'Estonie',
            'US' => 'États-Unis',
            'ET' => 'Éthiopie',
            'FK' => 'Falkland, Îles (Malvinas)',
            'FO' => 'Féroé, Îles',
            'FJ' => 'Fidji',
            'FI' => 'Finlande',
            'FR' => 'France',
            'GA' => 'Gabon',
            'GM' => 'Gambie',
            'GE' => 'Géorgie',
            'GS' => 'Géorgie Du Sud-Et-Les Îles Sandwich Du Sud',
            'GH' => 'Ghana',
            'GI' => 'Gibraltar',
            'GR' => 'Grèce',
            'GD' => 'Grenade',
            'GL' => 'Groenland',
            'GP' => 'Guadeloupe',
            'GU' => 'Guam',
            'GT' => 'Guatemala',
            'GG' => 'Guernesey',
            'GN' => 'Guinée',
            'GW' => 'Guinée-Bissau',
            'GQ' => 'Guinée Équatoriale',
            'GY' => 'Guyana',
            'GF' => 'Guyane Française',
            'HT' => 'Haïti',
            'HM' => 'Heard-Et-Îles Macdonald, Île',
            'HN' => 'Honduras',
            'HK' => 'Hong Kong',
            'HU' => 'Hongrie',
            'IM' => 'Île De Man',
            'UM' => 'Îles Mineures Éloignées Des États-Unis',
            'VG' => 'Îles Vierges Britanniques',
            'VI' => 'Îles Vierges Des États-Unis',
            'IN' => 'Inde',
            'ID' => 'Indonésie',
            'IR' => 'Iran, République Islamique D\'',
            'IQ' => 'Iraq',
            'IE' => 'Irlande',
            'IS' => 'Islande',
            'IL' => 'Israël',
            'IT' => 'Italie',
            'JM' => 'Jamaïque',
            'JP' => 'Japon',
            'JE' => 'Jersey',
            'JO' => 'Jordanie',
            'KZ' => 'Kazakhstan',
            'KE' => 'Kenya',
            'KG' => 'Kirghizistan',
            'KI' => 'Kiribati',
            'KW' => 'Koweït',
            'LA' => 'Lao, République Démocratique Populaire',
            'LS' => 'Lesotho',
            'LV' => 'Lettonie',
            'LB' => 'Liban',
            'LR' => 'Libéria',
            'LY' => 'Libye',
            'LI' => 'Liechtenstein',
            'LT' => 'Lituanie',
            'LU' => 'Luxembourg',
            'MO' => 'Macao',
            'MK' => 'Macédoine, L\'ex-République Yougoslave De',
            'MG' => 'Madagascar',
            'MY' => 'Malaisie',
            'MW' => 'Malawi',
            'MV' => 'Maldives',
            'ML' => 'Mali',
            'MT' => 'Malte',
            'MP' => 'Mariannes Du Nord, Îles',
            'MA' => 'Maroc',
            'MH' => 'Marshall, Îles',
            'MQ' => 'Martinique',
            'MU' => 'Maurice',
            'MR' => 'Mauritanie',
            'YT' => 'Mayotte',
            'MX' => 'Mexique',
            'FM' => 'Micronésie, États Fédérés De',
            'MD' => 'Moldova, République De',
            'MC' => 'Monaco',
            'MN' => 'Mongolie',
            'ME' => 'Monténégro',
            'MS' => 'Montserrat',
            'MZ' => 'Mozambique',
            'MM' => 'Myanmar',
            'NA' => 'Namibie',
            'NR' => 'Nauru',
            'NP' => 'Népal',
            'NI' => 'Nicaragua',
            'NE' => 'Niger',
            'NG' => 'Nigéria',
            'NU' => 'Niué',
            'NF' => 'Norfolk, Île',
            'NO' => 'Norvège',
            'NC' => 'Nouvelle-Calédonie',
            'NZ' => 'Nouvelle-Zélande',
            'IO' => 'Océan Indien, Territoire Britannique De L\'',
            'OM' => 'Oman',
            'UG' => 'Ouganda',
            'UZ' => 'Ouzbékistan',
            'PK' => 'Pakistan',
            'PW' => 'Palaos',
            'PS' => 'Palestinien Occupé, Territoire',
            'PA' => 'Panama',
            'PG' => 'Papouasie-Nouvelle-Guinée',
            'PY' => 'Paraguay',
            'NL' => 'Pays-Bas',
            'PE' => 'Pérou',
            'PH' => 'Philippines',
            'PN' => 'Pitcairn',
            'PL' => 'Pologne',
            'PF' => 'Polynésie Française',
            'PR' => 'Porto Rico',
            'PT' => 'Portugal',
            'QA' => 'Qatar',
            'RE' => 'Réunion',
            'RO' => 'Roumanie',
            'GB' => 'Royaume-Uni',
            'RU' => 'Russie, Fédération De',
            'RW' => 'Rwanda',
            'EH' => 'Sahara Occidental',
            'BL' => 'Saint-Barthélemy',
            'SH' => 'Sainte-Hélène, Ascension Et Tristan Da Cunha',
            'LC' => 'Sainte-Lucie',
            'KN' => 'Saint-Kitts-Et-Nevis',
            'SM' => 'Saint-Marin',
            'MF' => 'Saint-Martin(Partie Française)',
            'SX' => 'Saint-Martin (Partie Néerlandaise)',
            'PM' => 'Saint-Pierre-Et-Miquelon',
            'VA' => 'Saint-Siège (État De La Cité Du Vatican)',
            'VC' => 'Saint-Vincent-Et-Les Grenadines',
            'SB' => 'Salomon, Îles',
            'WS' => 'Samoa',
            'AS' => 'Samoa Américaines',
            'ST' => 'Sao Tomé-Et-Principe',
            'SN' => 'Sénégal',
            'RS' => 'Serbie',
            'SC' => 'Seychelles',
            'SL' => 'Sierra Leone',
            'SG' => 'Singapour',
            'SK' => 'Slovaquie',
            'SI' => 'Slovénie',
            'SO' => 'Somalie',
            'SD' => 'Soudan',
            'SS' => 'Soudan Du Sud',
            'LK' => 'Sri Lanka',
            'SE' => 'Suède',
            'CH' => 'Suisse',
            'SR' => 'Suriname',
            'SJ' => 'Svalbard Et Île Jan Mayen',
            'SZ' => 'Swaziland',
            'SY' => 'Syrienne, République Arabe',
            'TJ' => 'Tadjikistan',
            'TW' => 'Taïwan, Province De Chine',
            'TZ' => 'Tanzanie, République-Unie De',
            'TD' => 'Tchad',
            'CZ' => 'Tchèque, République',
            'TF' => 'Terres Australes Françaises',
            'TH' => 'Thaïlande',
            'TL' => 'Timor-Leste',
            'TG' => 'Togo',
            'TK' => 'Tokelau',
            'TO' => 'Tonga',
            'TT' => 'Trinité-Et-Tobago',
            'TN' => 'Tunisie',
            'TM' => 'Turkménistan',
            'TC' => 'Turks-Et-Caïcos, Îles',
            'TR' => 'Turquie',
            'TV' => 'Tuvalu',
            'UA' => 'Ukraine',
            'UY' => 'Uruguay',
            'VU' => 'Vanuatu',
            'VE' => 'Venezuela, République Bolivarienne Du',
            'VN' => 'Viet Nam',
            'WF' => 'Wallis Et Futuna',
            'YE' => 'Yémen',
            'ZM' => 'Zambie',
            'ZW' => 'Zimbabwe',
        ],
    ];

    /**
     * @var string the associated database table name
     */
    public static $tableName;

    /**
     * @var int HumHub Group ID
     */
    public $humhubGroupId;

    /**
     * @var bool
     */
    public $_tableExists = false;

    /**
     * @inerhitdoc
     */
    public function __construct($config = [])
    {
        // Reinitialize because value is the same as the previous instance
        $this->_tableExists = false;

        // Get table name
        $yeswiki = Yeswiki::findOne($config['humhubGroupId'] ?? $this->humhubGroupId);
        if ($yeswiki === null || !$yeswiki->table_prefix) {
            return;
        }
        static::$tableName = $yeswiki->table_prefix . self::TABLE_NAME_SUFFIX;

        // Check if table exists
        try {
            $tableSchema = static::getDb()
                ->getSchema()
                ->getTableSchema(static::tableName());
        } catch (NotSupportedException $e) {
            return;
        }
        $this->_tableExists = $tableSchema !== null;

        if ($this->_tableExists) {
            parent::__construct($config);
        }
    }

    public static function getDb()
    {
        return Yii::$app->dbYesWiki;
    }

    public static function tableName()
    {
        return static::$tableName;
    }


    /*
        public function attributeLabels()
        {
            return [
            ];
        }

        public function rules()
        {
           return [
           ];
        }
    */

    // If YesWiki profile doesn't exist, create it.
    // And update YesWiki profile fields
    /**
     * @param Group $humhubGroup
     * @param UserNotExtended $humhubUser
     * @return bool
     * @throws NotSupportedException
     */
    public static function addOrUpdateProfile($humhubGroup, $humhubUser)
    {
        /** @var Module $module */
        $module = Yii::$app->getModule('yeswiki');

        if ($humhubUser->status !== User::STATUS_ENABLED) {
            return false;
        }

        // If profile not yet created
        if ($humhubUser->profile === null) {
            return false;
        }

        // Create model with dynamically created table name from humhub group and check if table exists
        $yesWikiPagesModel = new self(['humhubGroupId' => $humhubGroup->id]);
        if (!$yesWikiPagesModel->_tableExists) {
            return false;
        }

        // Get $pageTag from username
        $pageTag = $humhubUser->username;

        // Check if user exists (while user deletion process, this function is exectued, but YesWiki user is already deleted, so we don't want to recreate profile of a deleted user)
        $yesWikiUsersModel = new YesWikiUsers(['humhubGroupId' => $humhubGroup->id]);
        if ($yesWikiUsersModel::findOne(['name' => $pageTag]) === null) {
            return false;
        }

        // Convert HumHub profile fields to YesWiki profile fields and tags
        $yeswikiProfileFieldsAndTags = self::humhubProfile2YeswikiProfileFieldsAndTags($humhubUser);
        $yesWikiProfileFields = $yeswikiProfileFieldsAndTags['yesWikiProfileFields'];
        $yesWikiTags = $yeswikiProfileFieldsAndTags['yesWikiTags'];

        // Get profile in YesWiki
        $yesWikiPage = $yesWikiPagesModel::find()
            ->andWhere(['like', 'body', '"id_typeannonce":"' . $module->yesWikiProfileFormId . '"'])
            ->andWhere(['tag' => $pageTag])
            ->andWhere(['latest' => 'Y'])
            ->one();

        // If profile doesn't exists
        if ($yesWikiPage === null) {
            // Create it
            self::insertPage($humhubGroup, $pageTag, $yesWikiProfileFields);
            YesWikiAcls::insertRights($humhubGroup, $pageTag);
            YesWikiTriples::insertFicheBazar($humhubGroup, $pageTag);
        } // If profile exists
        else {
            // Updated it
            $yesWikiPage->time = date("Y-m-d H:i:s");
            $yesWikiPage->body = json_encode($yesWikiProfileFields);
            $yesWikiPage->save();
        }

        // Add or Update tags in triples table
        YesWikiTriples::updateTags($humhubGroup, $pageTag, $yesWikiTags);

        return true;
    }

    /**
     * @param UserNotExtended $humhubUser
     * @return array
     */
    private static function humhubProfile2YeswikiProfileFieldsAndTags($humhubUser)
    {
        /** @var Module $module */
        $module = Yii::$app->getModule('yeswiki');

        $pageTag = $humhubUser->username;

        // Create YesWiki profile fields
        $yesWikiProfileFields = [
            'id_typeannonce' => (string)$module->yesWikiProfileFormId,
            'id_fiche' => $pageTag,
            'createur' => $pageTag,
            'statut_fiche' => '1',
            'date_creation_fiche' => (string)date("Y-m-d H:i:s"),
            'date_maj_fiche' => (string)date("Y-m-d H:i:s"),
            'data-imagebf_image' => '',
            'filename-imagebf_image' => '',
            'bf_email' => $humhubUser->email,
            'bf_titre' => str_ireplace("'", "’", $humhubUser->profile->firstname . ' ' . $humhubUser->profile->lastname),
            'bf_url_photo' => Url::to(['/uploads/profile_image/' . $humhubUser->guid . '_org.jpg'], true),
        ];

        if (
            $module->profileFieldYesWikiLatLng
            && Yii::$app->getModule('members-map') !== null
        ) {
            // Add coordinates at a zip scale
            $geocodingResult = Geocode::getGeocodingResult(Profile::findOne(['user_id' => $humhubUser->id]), true);
            if ($geocodingResult->status === GeocodingResult::STATUS_SUCCESS) {
                $yesWikiProfileFields['bf_latitude'] = $geocodingResult->lat;
                $yesWikiProfileFields['bf_longitude'] = $geocodingResult->lng;
                $yesWikiProfileFields['carte_google'] = $geocodingResult->lat . '|' . $geocodingResult->lng;
            }
        }

        // Add other profile fields
        $yesWikiTags = [];
        foreach (static::getProfileFieldsHumhub2YesWiki() as $humHubField => $yesWikiField) {

            // Skip if field doesn't exist in tables
            if (!isset($humhubUser->profile->$humHubField)) {
                $yesWikiProfileFields[$yesWikiField] = '';
            } else {
                $yesWikiProfileFields[$yesWikiField] = self::profileValHumHub2YesWiki(
                    $humHubField,
                    $humhubUser->profile->$humHubField
                );
                // Get tags
                if ($yesWikiField === 'bf_tags') {
                    $yesWikiTags = explode(',', (string)$humhubUser->profile->$humHubField);
                }
            }
        }

        return [
            'yesWikiProfileFields' => $yesWikiProfileFields,
            'yesWikiTags' => $yesWikiTags,
        ];
    }

    /**
     * For all sites :
     * - If YesWiki profile doesn't exist, create it.
     * - Update YesWiki profile fields
     * @param UserNotExtended $humhubUser
     * @return void
     */
    public static function addOrUpdateProfileInAllSites($humhubUser)
    {
        foreach ($humhubUser->syncGroups as $humhubGroup) {
            self::addOrUpdateProfile($humhubGroup, $humhubUser);
        }
    }


    public static function insertPage($humhubGroup, $pageTag, $yesWikiProfileFields)
    {
        // Create model with dynamically created table name from humhub group and check if table exists
        $yesWikiPagesModel = new self(['humhubGroupId' => $humhubGroup->id]);
        if (!$yesWikiPagesModel->_tableExists) {
            return false;
        }

        // Create Page
        $yesWikiPage = new self(['humhubGroupId' => $humhubGroup->id]);
        $yesWikiPage->tag = $pageTag;
        $yesWikiPage->time = date("Y-m-d H:i:s");
        $yesWikiPage->owner = $pageTag;
        $yesWikiPage->user = $pageTag;
        $yesWikiPage->body = json_encode($yesWikiProfileFields);
        $yesWikiPage->body_r = '';
        $yesWikiPage->latest = 'Y';
        $yesWikiPage->save(false);

        return true;
    }


    /**
     * Update a YesWiki profile field
     * @param Group $humhubGroup
     * @param $username
     * @param $field
     * @param $newValue
     * @return bool
     * @throws NotSupportedException
     */
    public static function updateProfileField($humhubGroup, $username, $field, $newValue)
    {
        /** @var Module $module */
        $module = Yii::$app->getModule('yeswiki');

        // Create model with dynamically created table name from humhub group and check if table exists
        $yesWikiPagesModel = new self(['humhubGroupId' => $humhubGroup->id]);
        if (!$yesWikiPagesModel->_tableExists) {
            return false;
        }

        // Get profile in YesWiki
        $yesWikiPage = $yesWikiPagesModel::find()
            ->andWhere(['like', 'body', '"id_typeannonce":"' . $module->yesWikiProfileFormId . '"'])
            ->andWhere(['tag' => $username])
            ->andWhere(['latest' => 'Y'])
            ->one();

        if ($yesWikiPage === null) {
            return false;
        }

        $yesWikiProfileFields = (array)json_decode((string)$yesWikiPage->body);

        if (!isset($yesWikiProfileFields[$field])) {
            return false;
        }

        // Set new value
        $yesWikiProfileFields[$field] = $newValue;

        // Save new values in YesWiki
        $yesWikiPage->body = json_encode($yesWikiProfileFields);
        $yesWikiPage->save(false);

        return true;
    }

    /**
     * @param Group $humhubGroup
     * @param $oldPageTag
     * @param $newPageTag
     * @return bool
     * @throws \Throwable
     * @throws NotSupportedException
     * @throws \yii\db\StaleObjectException
     */
    public static function updateTag($humhubGroup, $oldPageTag, $newPageTag)
    {
        // Create model with dynamically created table name from humhub group and check if table exists
        $yesWikiPagesModel = new self(['humhubGroupId' => $humhubGroup->id]);
        if (!$yesWikiPagesModel->_tableExists) {
            return false;
        }

        // If tags already exist with the new name, delete them
        $yesWikiPagesQuery = $yesWikiPagesModel::find()
            ->where([
                'or',
                ['tag' => $newPageTag],
                ['owner' => $newPageTag],
                ['user' => $newPageTag],
                ['comment_on' => $newPageTag],
            ]);
        foreach ($yesWikiPagesQuery->each(1000) as $yesWikiPage) {
            $yesWikiPage->delete();
        }

        // Update with new name
        $yesWikiPagesQuery = $yesWikiPagesModel::find()
            ->where(['tag' => $oldPageTag]);
        foreach ($yesWikiPagesQuery->each(1000) as $yesWikiPage) {
            $yesWikiPage->tag = $newPageTag;
            $yesWikiPage->owner = $newPageTag;
            $yesWikiPage->user = $newPageTag;
            $yesWikiPage->save();
        }
        $yesWikiPagesQuery = $yesWikiPagesModel::find()
            ->where(['comment_on' => $oldPageTag]);
        foreach ($yesWikiPagesQuery->each(1000) as $yesWikiPage) {
            $yesWikiPage->comment_on = $newPageTag;
            $yesWikiPage->save();
        }

        self::updateProfileField($humhubGroup, $newPageTag, 'id_fiche', $newPageTag);
        self::updateProfileField($humhubGroup, $newPageTag, 'createur', $newPageTag);

        return true;
    }

    /**
     * @param Group $humhubGroup
     * @param $pageTag
     * @return bool
     * @throws \Throwable
     * @throws NotSupportedException
     * @throws \yii\db\StaleObjectException
     */
    public static function deleteTag($humhubGroup, $pageTag)
    {
        // Create model with dynamically created table name from humhub group and check if table exists
        $yesWikiPagesModel = new self(['humhubGroupId' => $humhubGroup->id]);
        if (!$yesWikiPagesModel->_tableExists) {
            return false;
        }

        $yesWikiPagesQuery = $yesWikiPagesModel::find()
            ->where(['tag' => $pageTag]);
        foreach ($yesWikiPagesQuery->each(1000) as $yesWikiPage) {
            $yesWikiPage->delete();
        }

        return true;
    }


    /**
     * @param Group $humhubGroup
     * @return bool
     * @throws \Throwable
     * @throws NotSupportedException
     * @throws \yii\db\StaleObjectException
     */
    public static function deleteAllProfileEntries($humhubGroup)
    {
        /** @var Module $module */
        $module = Yii::$app->getModule('yeswiki');

        // Create model with dynamically created table name from humhub group and check if table exists
        $yesWikiPagesModel = new self(['humhubGroupId' => $humhubGroup->id]);
        if (!$yesWikiPagesModel->_tableExists) {
            return false;
        }

        $yesWikiPagesQuery = $yesWikiPagesModel::find()
            ->where(['like', 'body', '"id_typeannonce":"' . $module->yesWikiProfileFormId . '"']);
        foreach ($yesWikiPagesQuery->each(1000) as $yesWikiPage) {
            $yesWikiPage->delete();
        }

        return true;
    }


    /**
     * @param string $humHubField
     * @param string $humHubFieldVal
     * @return string
     */
    private static function profileValHumHub2YesWiki($humHubField, $humHubFieldVal)
    {
        $profileValHumhub2YesWiki = static::getProfileValHumhub2YesWiki();
        if (isset($profileValHumhub2YesWiki[$humHubField])) {
            $yesWikiVals = [];
            foreach (explode(',', (string)$humHubFieldVal) as $humHubVal) {
                if (isset($profileValHumhub2YesWiki[$humHubField][$humHubVal])) {
                    $yesWikiVals[] = $profileValHumhub2YesWiki[$humHubField][$humHubVal];
                }
            }
            return implode(',', $yesWikiVals);
        }

        return $humHubFieldVal;
    }

    /**
     * @return array|string[]
     */
    public static function getProfileFieldsHumhub2YesWiki()
    {
        /** @var Module $module */
        $module = Yii::$app->getModule('yeswiki');
        return $module->extraProfileFieldsHumhub2YesWiki;
    }

    /**
     * @return array|string[]
     */
    public static function getProfileValHumhub2YesWiki()
    {
        /** @var Module $module */
        $module = Yii::$app->getModule('yeswiki');
        return array_merge(self::PROFILE_VAL_HUMHUB_2_YESWIKI, $module->extraProfileValHumhub2YesWiki);
    }
}
