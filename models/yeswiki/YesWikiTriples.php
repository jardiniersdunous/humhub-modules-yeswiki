<?php
/**
 * YesWiki
 * @link https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki
 * @license https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki/blob/main/docs/LICENCE.md
 * @author [Marc Farré](https://marc.fun)
 */

namespace humhub\modules\yeswiki\models\yeswiki;

use humhub\components\ActiveRecord;
use humhub\modules\space\models\Space;
use humhub\modules\user\models\Group;
use humhub\modules\yeswiki\models\Yeswiki;
use Yii;
use yii\base\NotSupportedException;
use yii\db\StaleObjectException;


/**
 * @property int $id
 * @property string $resource
 * @property string $property
 * @property string $value
 */
class YesWikiTriples extends ActiveRecord
{
    const TABLE_NAME_SUFFIX = 'triples';

    // For groups synchronized with HumHub spaces members, use these prefix and suffix :
    const SPACE_PREFIX = 'humhub-'; // Must be compatible with YesWiki tag naming
    const SPACE_MEMBER_SUFFIX = '';
    const SPACE_ADMIN_SUFFIX = '-admins'; // Must be compatible with YesWiki tag naming

    const PROPERTY_TYPE = 'http://outils-reseaux.org/_vocabulary/type';
    const PROPERTY_TAG = 'http://outils-reseaux.org/_vocabulary/tag';
    const PROPERTY_ACLS = 'http://www.wikini.net/_vocabulary/acls';
    const NON_DATA_PROPERTIES = [
        self::PROPERTY_TYPE,
        self::PROPERTY_TAG,
        self::PROPERTY_ACLS,
    ];

    /**
     * @var string the associated database table name
     */
    public static $tableName;

    /**
     * @var int HumHub Group ID
     */
    public $humhubGroupId;

    /**
     * @var bool
     */
    public $_tableExists = false;

    /**
     * @inerhitdoc
     */
    public function __construct($config = [])
    {
        // Reinitialize because value is the same as the previous instance
        $this->_tableExists = false;

        // Get table name
        $yeswiki = Yeswiki::findOne($config['humhubGroupId'] ?? $this->humhubGroupId);
        if ($yeswiki === null || !$yeswiki->table_prefix) {
            return;
        }
        static::$tableName = $yeswiki->table_prefix . self::TABLE_NAME_SUFFIX;

        // Check if table exists
        try {
            $tableSchema = static::getDb()
                ->getSchema()
                ->getTableSchema(static::tableName());
        } catch (NotSupportedException $e) {
            return;
        }
        $this->_tableExists = $tableSchema !== null;

        if ($this->_tableExists) {
            parent::__construct($config);
        }
    }

    public static function getDb()
    {
        return Yii::$app->dbYesWiki;
    }

    public static function tableName()
    {
        return static::$tableName;
    }

    /**
     * @param Group $humhubGroup
     * @param $pageTag
     * @return bool
     */
    public static function insertFicheBazar($humhubGroup, $pageTag)
    {
        $yesWikiTriplesModel = new self(['humhubGroupId' => $humhubGroup->id]);
        if (!$yesWikiTriplesModel->_tableExists) {
            return false;
        }

        $yesWikiTriple = $yesWikiTriplesModel::find()
            ->where([
                'and',
                ['resource' => $pageTag],
                ['property' => self::PROPERTY_TYPE],
                ['value' => 'fiche_bazar'],
            ])
            ->one();
        if ($yesWikiTriple === null) {
            $yesWikiTriple = new self(['humhubGroupId' => $humhubGroup->id]);
            $yesWikiTriple->resource = $pageTag;
            $yesWikiTriple->property = self::PROPERTY_TYPE;
            $yesWikiTriple->value = 'fiche_bazar';
            $yesWikiTriple->save();
        }
        return true;
    }

    /**
     * @param Group $humhubGroup
     * @param $pageTag
     * @param $tags
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public static function updateTags($humhubGroup, $pageTag, $tags)
    {
        $yesWikiTriplesModel = new self(['humhubGroupId' => $humhubGroup->id]);
        if (!$yesWikiTriplesModel->_tableExists) {
            return false;
        }

        // Remove all old tags
        $yesWikiTriples = $yesWikiTriplesModel::find()
            ->where([
                'and',
                ['resource' => $pageTag],
                ['property' => self::PROPERTY_TAG],
            ])
            ->all();
        foreach ($yesWikiTriples as $yesWikiTripe) {
            $yesWikiTripe->delete();
        }

        // Add new tags
        foreach ($tags as $tag) {
            if (!empty($tag)) {
                $yesWikiTripe = new self(['humhubGroupId' => $humhubGroup->id]);
                $yesWikiTripe->resource = $pageTag;
                $yesWikiTripe->property = self::PROPERTY_TAG;
                $yesWikiTripe->value = $tag;
                $yesWikiTripe->save();
            }
        }

        return true;
    }

    /**
     * @param Group $humhubGroup
     * @param $oldPageTag
     * @param $newPageTag
     * @param bool $keepData If true, delete only the entries related to the properties recreated (especially, keep the entries with property `https://yeswiki.net/vocabulary/progress`)
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public static function updateTag($humhubGroup, $oldPageTag, $newPageTag, $keepData = true)
    {
        $yesWikiTriplesModel = new self(['humhubGroupId' => $humhubGroup->id]);
        if (!$yesWikiTriplesModel->_tableExists) {
            return false;
        }

        // If tags already exist with the new name, delete them
        static::deleteTag($humhubGroup, $newPageTag, $keepData);

        $yesWikiTriples = $yesWikiTriplesModel::findAll(['resource' => $oldPageTag]);
        foreach ($yesWikiTriples as $yesWikiTriple) {
            $yesWikiTriple->resource = $newPageTag;
            $yesWikiTriple->save();
        }

        return true;
    }

    /**
     * @param Group $humhubGroup
     * @param $pageTag
     * @param bool $keepData If true, delete only the entries related to the properties recreated (especially, keep the entries with property `https://yeswiki.net/vocabulary/progress`)
     * @return bool
     * @throws StaleObjectException
     * @throws \Throwable
     */
    public static function deleteTag($humhubGroup, $pageTag, $keepData = true)
    {
        $yesWikiTriplesModel = new self(['humhubGroupId' => $humhubGroup->id]);
        if (!$yesWikiTriplesModel->_tableExists) {
            return false;
        }

        $yesWikiTriplesQuery = $yesWikiTriplesModel::find()
            ->where(['resource' => $pageTag]);
        if ($keepData) {
            $yesWikiTriplesQuery->andWhere(['property' => self::NON_DATA_PROPERTIES]);
        }
        /** @var self $yesWikiTriple */
        foreach ($yesWikiTriplesQuery->each(1000) as $yesWikiTriple) {
            $yesWikiTriple->delete();
        }

        return true;
    }

    /**
     * @param Group $humhubGroup
     * @param $humhubSpace
     * @param $isAdmin
     * @return bool
     */
    public static function insertGroup($humhubGroup, $humhubSpace, $isAdmin = false)
    {
        $yesWikiTriplesModel = new self(['humhubGroupId' => $humhubGroup->id]);
        if (!$yesWikiTriplesModel->_tableExists) {
            return false;
        }

        $groupName = self::groupNameFromHumhubSpace($humhubSpace, $isAdmin);

        $yesWikiTriple = $yesWikiTriplesModel::find()
            ->where([
                'and',
                ['resource' => 'ThisWikiGroup:' . $groupName],
                ['property' => self::PROPERTY_ACLS],
            ])
            ->one();

        if ($yesWikiTriple !== null) {
            return false;
        }

        $yesWikiTriple = new self(['humhubGroupId' => $humhubGroup->id]);
        $yesWikiTriple->resource = 'ThisWikiGroup:' . $groupName;
        $yesWikiTriple->property = self::PROPERTY_ACLS;
        $yesWikiTriple->value = '';
        $yesWikiTriple->save();

        return true;
    }

    /**
     * @param Group $humhubGroup
     * @param Space $humhubSpace
     * @param bool $isAdmin
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public static function deleteGroup($humhubGroup, $humhubSpace, $isAdmin = false)
    {
        $yesWikiTriplesModel = new self(['humhubGroupId' => $humhubGroup->id]);
        if (!$yesWikiTriplesModel->_tableExists) {
            return false;
        }

        $groupName = self::groupNameFromHumhubSpace($humhubSpace, $isAdmin);

        $yesWikiTriple = $yesWikiTriplesModel::find()
            ->where([
                'and',
                ['resource' => 'ThisWikiGroup:' . $groupName],
                ['property' => self::PROPERTY_ACLS],
            ])
            ->one();

        if ($yesWikiTriple === null) {
            return false;
        }

        $yesWikiTriple->delete();

        return true;
    }

    /**
     * @param Group $humhubGroup
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public static function deleteAllGroups($humhubGroup)
    {
        $yesWikiTriplesModel = new self(['humhubGroupId' => $humhubGroup->id]);
        if (!$yesWikiTriplesModel->_tableExists) {
            return false;
        }

        $yesWikiTriplesQuery = $yesWikiTriplesModel::find()
            ->where([
                'and',
                ['like', 'resource', 'ThisWikiGroup:' . self::SPACE_PREFIX . '%', false],
                ['property' => self::PROPERTY_ACLS],
            ]);

        foreach ($yesWikiTriplesQuery->each(1000) as $yesWikiTriple) {
            $yesWikiTriple->delete();
        }

        return true;
    }

    /**
     * @param Group $humhubGroup
     * @param $oldHumhubSpace
     * @param $newHumhubSpace
     * @param $isAdmin
     * @return bool
     */
    public static function updateGroupName($humhubGroup, $oldHumhubSpace, $newHumhubSpace, $isAdmin = false)
    {
        $yesWikiTriplesModel = new self(['humhubGroupId' => $humhubGroup->id]);
        if (!$yesWikiTriplesModel->_tableExists) {
            return false;
        }

        $oldGroupName = self::groupNameFromHumhubSpace($oldHumhubSpace, $isAdmin);
        $newGroupName = self::groupNameFromHumhubSpace($newHumhubSpace, $isAdmin);

        $yesWikiTriple = $yesWikiTriplesModel::find()
            ->where([
                'and',
                ['resource' => 'ThisWikiGroup:' . $oldGroupName],
                ['property' => self::PROPERTY_ACLS],
            ])
            ->one();

        if ($yesWikiTriple === null) {
            return false;
        }

        $yesWikiTriple->resource = 'ThisWikiGroup:' . $newGroupName;
        $yesWikiTriple->save();

        return true;
    }

    /**
     * @param Group $humhubGroup
     * @param Space $humhubSpace
     * @param $usernameToAdd
     * @param $isAdmin
     * @return bool
     */
    public static function addMemberInGroup($humhubGroup, $humhubSpace, $usernameToAdd, $isAdmin = false)
    {
        $yesWikiTriplesModel = new self(['humhubGroupId' => $humhubGroup->id]);
        if (!$yesWikiTriplesModel->_tableExists) {
            return false;
        }

        // Create YesWiki group if doesn't exist
        self::insertGroup($humhubGroup, $humhubSpace, $isAdmin);

        $groupName = self::groupNameFromHumhubSpace($humhubSpace, $isAdmin);

        $yesWikiTriple = $yesWikiTriplesModel::find()
            ->where([
                'and',
                ['resource' => 'ThisWikiGroup:' . $groupName],
                ['property' => self::PROPERTY_ACLS],
            ])
            ->one();

        if ($yesWikiTriple === null) {
            return false;
        }

        // Create users list
        $groupUsers = array_filter(explode("\n", (string)$yesWikiTriple['value']));

        // Check if $usernameToAdd already exists in group
        if (in_array($usernameToAdd, $groupUsers)) {
            return false;
        }

        // Add user and save
        $groupUsers[] = $usernameToAdd;
        $yesWikiTriple->value = implode("\n", $groupUsers);
        $yesWikiTriple->save();

        return true;
    }

    /**
     * @param Group $humhubGroup
     * @param Space $humhubSpace
     * @param $usernameToDelete
     * @param $isAdmin
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public static function removeMemberInGroup($humhubGroup, $humhubSpace, $usernameToDelete, $isAdmin = false)
    {
        $yesWikiTriplesModel = new self(['humhubGroupId' => $humhubGroup->id]);

        if (!$yesWikiTriplesModel->_tableExists) {
            return false;
        }

        $groupName = self::groupNameFromHumhubSpace($humhubSpace, $isAdmin);

        $yesWikiTriple = $yesWikiTriplesModel::find()
            ->where([
                'and',
                ['resource' => 'ThisWikiGroup:' . $groupName],
                ['property' => self::PROPERTY_ACLS],
            ])
            ->one();

        if ($yesWikiTriple === null) {
            return false;
        }

        // Create users list
        $groupUsers = array_filter(explode("\n", (string)$yesWikiTriple['value']));

        // Check if $usernameToDelete already exists in group
        $groupUserKey = array_search($usernameToDelete, $groupUsers);
        if ($groupUserKey === false) {
            return false;
        }

        // Remove user
        unset($groupUsers[$groupUserKey]);

        // If they are still remaining members
        if (count($groupUsers) > 0) {
            // Save new members list
            $yesWikiTriple->value = implode("\n", $groupUsers);
            $yesWikiTriple->save();
        } // If group has no member anymore
        else {
            // Delete group
            self::deleteGroup($humhubGroup, $humhubSpace, $isAdmin);
        }

        return true;
    }

    /**
     * @param Group $humhubGroup
     * @param Space $humhubSpace
     * @param $oldUsername
     * @param $newUsername
     * @param $isAdmin
     * @return bool
     */
    public static function updateMemberInGroup($humhubGroup, $humhubSpace, $oldUsername, $newUsername, $isAdmin = false)
    {
        $yesWikiTriplesModel = new self(['humhubGroupId' => $humhubGroup->id]);
        if (!$yesWikiTriplesModel->_tableExists) {
            return false;
        }

        $groupName = self::groupNameFromHumhubSpace($humhubSpace, $isAdmin);

        $yesWikiTriple = $yesWikiTriplesModel::find()
            ->where([
                'and',
                ['resource' => 'ThisWikiGroup:' . $groupName],
                ['property' => self::PROPERTY_ACLS],
                ['like', 'value', $oldUsername],
            ])
            ->one();

        if ($yesWikiTriple === null) {
            return false;
        }

        // Create users list
        $groupUsers = array_filter(explode("\n", (string)$yesWikiTriple['value']));

        if (!in_array($oldUsername, $groupUsers)) {
            return false;
        }

        // Rename username
        $key = array_search($oldUsername, $groupUsers);
        $groupUsers[$key] = $newUsername;

        // Save group
        $yesWikiTriple->value = implode("\n", $groupUsers);
        $yesWikiTriple->save();

        return true;
    }

    /**
     * @param Space $humhubSpace
     * @param $isAdmin
     * @return string
     */
    public static function groupNameFromHumhubSpace($humhubSpace, $isAdmin = false)
    {
        return
            self::SPACE_PREFIX .
            $humhubSpace->url .
            ($isAdmin ? self::SPACE_ADMIN_SUFFIX : self::SPACE_MEMBER_SUFFIX);
    }


    /*
        public function attributeLabels()
        {
            return [
            ];
        }

        public function rules()
        {
           return [
           ];
        }
    */
}

?>
