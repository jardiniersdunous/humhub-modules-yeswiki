<?php
/**
 * YesWiki
 * @link https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki
 * @license https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki/blob/main/docs/LICENCE.md
 * @author [Marc Farré](https://marc.fun)
 */

namespace humhub\modules\yeswiki\models\yeswiki;

use humhub\components\ActiveRecord;
use humhub\modules\user\models\Group;
use humhub\modules\yeswiki\models\Yeswiki;
use Yii;
use yii\base\NotSupportedException;

/**
 * @property string $page_tag
 * @property string $privilege
 * @property string $list
 */
class YesWikiAcls extends ActiveRecord
{
    const TABLE_NAME_SUFFIX = 'acls';

    /**
     * @var string the associated database table name
     */
    public static $tableName;

    /**
     * @var int HumHub Group ID
     */
    public $humhubGroupId;

    /**
     * @var bool
     */
    public $_tableExists = false;

    /**
     * @inerhitdoc
     */
    public function __construct($config = [])
    {
        // Reinitialize because value is the same as the previous instance
        $this->_tableExists = false;

        // Get table name
        $yeswiki = Yeswiki::findOne($config['humhubGroupId'] ?? $this->humhubGroupId);
        if ($yeswiki === null || !$yeswiki->table_prefix) {
            return;
        }
        static::$tableName = $yeswiki->table_prefix . self::TABLE_NAME_SUFFIX;

        // Check if table exists
        try {
            $tableSchema = static::getDb()
                ->getSchema()
                ->getTableSchema(static::tableName());
        } catch (NotSupportedException $e) {
            return;
        }
        $this->_tableExists = $tableSchema !== null;

        if ($this->_tableExists) {
            parent::__construct($config);
        }
    }

    public static function getDb()
    {
        return Yii::$app->dbYesWiki;
    }

    public static function tableName()
    {
        return static::$tableName;
    }

    /**
     * @param Group $humhubGroup
     * @param string $pageTag
     * @return false|void
     */
    public static function insertRights($humhubGroup, $pageTag)
    {
        $yesWikiAclsModel = new self(['humhubGroupId' => $humhubGroup->id]);
        if (!$yesWikiAclsModel->_tableExists) {
            return false;
        }

        $yesWikiAcl = $yesWikiAclsModel::find()
            ->where(['page_tag' => $pageTag])
            ->one();

        if ($yesWikiAcl === null) {
            // + = connected users only; * = everybody

            $yesWikiAcl = new self(['humhubGroupId' => $humhubGroup->id]);
            $yesWikiAcl->page_tag = $pageTag;
            $yesWikiAcl->privilege = 'read';
            $yesWikiAcl->list =
                (empty($humhubGroup->defaultSpaces[0])) ? '@admins' :
                    '@' . YesWikiTriples::groupNameFromHumhubSpace($humhubGroup->defaultSpaces[0]);
            $yesWikiAcl->save();

            $yesWikiAcl = new self(['humhubGroupId' => $humhubGroup->id]);
            $yesWikiAcl->page_tag = $pageTag;
            $yesWikiAcl->privilege = 'write';
            $yesWikiAcl->list = '@admins';
            $yesWikiAcl->save();

            $yesWikiAcl = new self(['humhubGroupId' => $humhubGroup->id]);
            $yesWikiAcl->page_tag = $pageTag;
            $yesWikiAcl->privilege = 'comment';
            $yesWikiAcl->list = '@admins';
            $yesWikiAcl->save();
        }
    }

    /**
     * @param Group $humhubGroup
     * @param string $oldPageTag
     * @param string $newPageTag
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public static function updateTag($humhubGroup, $oldPageTag, $newPageTag)
    {
        $yesWikiAclsModel = new self(['humhubGroupId' => $humhubGroup->id]);
        if (!$yesWikiAclsModel->_tableExists) {
            return false;
        }

        // If tags already exist with the new name, delete them
        $yesWikiAcls = $yesWikiAclsModel::findAll(['page_tag' => $newPageTag]);
        foreach ($yesWikiAcls as $yesWikiAcl) {
            $yesWikiAcl->delete();
        }

        // Update tag name
        $yesWikiAcls = $yesWikiAclsModel::findAll(['page_tag' => $oldPageTag]);
        foreach ($yesWikiAcls as $yesWikiAcl) {
            $yesWikiAcl->page_tag = $newPageTag;
            $yesWikiAcl->save();
        }

        return true;
    }

    /**
     * @param Group $humhubGroup
     * @param string $pageTag
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public static function deleteTag($humhubGroup, $pageTag)
    {
        $yesWikiAclsModel = new self(['humhubGroupId' => $humhubGroup->id]);
        if (!$yesWikiAclsModel->_tableExists) {
            return false;
        }

        $yesWikiAcls = $yesWikiAclsModel::findAll(['page_tag' => $pageTag]);
        foreach ($yesWikiAcls as $yesWikiAcl) {
            $yesWikiAcl->delete();
        }

        return true;
    }


    /*
        public function attributeLabels()
        {
            return [
            ];
        }

        public function rules()
        {
           return [
           ];
        }
    */
}
