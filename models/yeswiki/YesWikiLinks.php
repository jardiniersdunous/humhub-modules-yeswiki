<?php
/**
 * YesWiki
 * @link https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki
 * @license https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki/blob/main/docs/LICENCE.md
 * @author [Marc Farré](https://marc.fun)
 */

namespace humhub\modules\yeswiki\models\yeswiki;

use humhub\components\ActiveRecord;
use humhub\modules\yeswiki\models\Yeswiki;
use Yii;
use yii\base\NotSupportedException;


/**
 * @property string $from_tag
 * @property string $to_tag
 */
class YesWikiLinks extends ActiveRecord
{
    const TABLE_NAME_SUFFIX = 'links';

    /**
     * @var string the associated database table name
     */
    public static $tableName;

    /**
     * @var int HumHub Group ID
     */
    public $humhubGroupId;

    /**
     * @var bool
     */
    public $_tableExists = false;

    /**
     * @inerhitdoc
     */
    public function __construct($config = [])
    {
        // Reinitialize because value is the same as the previous instance
        $this->_tableExists = false;

        // Get table name
        $yeswiki = Yeswiki::findOne($config['humhubGroupId'] ?? $this->humhubGroupId);
        if ($yeswiki === null || !$yeswiki->table_prefix) {
            return;
        }
        static::$tableName = $yeswiki->table_prefix . self::TABLE_NAME_SUFFIX;

        // Check if table exists
        try {
            $tableSchema = static::getDb()
                ->getSchema()
                ->getTableSchema(static::tableName());
        } catch (NotSupportedException $e) {
            return;
        }
        $this->_tableExists = $tableSchema !== null;

        if ($this->_tableExists) {
            parent::__construct($config);
        }
    }

    public static function getDb()
    {
        return Yii::$app->dbYesWiki;
    }

    public static function tableName()
    {
        return static::$tableName;
    }


    public static function updateTag($humhubGroup, $oldPageTag, $newPageTag)
    {
        $yesWikiLinksModel = new self(['humhubGroupId' => $humhubGroup->id]);
        if (!$yesWikiLinksModel->_tableExists) {
            return false;
        }

        // If tags already exist with the new name, delete them
        $yesWikiLinks = $yesWikiLinksModel::findAll(['from_tag' => $newPageTag]);
        foreach ($yesWikiLinks as $yesWikiLink) {
            $yesWikiLink->delete();
        }

        $yesWikiLinks = $yesWikiLinksModel::findAll(['from_tag' => $oldPageTag]);
        foreach ($yesWikiLinks as $yesWikiLink) {
            $yesWikiLink->from_tag = $newPageTag;
            $yesWikiLink->save();
        }

        return true;
    }


    public static function deleteTag($humhubGroup, $pageTag)
    {
        $yesWikiLinksModel = new self(['humhubGroupId' => $humhubGroup->id]);
        if (!$yesWikiLinksModel->_tableExists) {
            return false;
        }

        $yesWikiLinks = $yesWikiLinksModel::findAll(['from_tag' => $pageTag]);
        foreach ($yesWikiLinks as $yesWikiLink) {
            $yesWikiLink->delete();
        }

        return true;
    }


    /*
        public function attributeLabels()
        {
            return [
            ];
        }

        public function rules()
        {
           return [
           ];
        }
    */
}

?>
