<?php
/**
 * YesWiki
 * @link https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki
 * @license https://gitlab.com/jardiniersdunous/humhub-modules-yeswiki/blob/main/docs/LICENCE.md
 * @author [Marc Farré](https://marc.fun)
 */

namespace humhub\modules\yeswiki\models\yeswiki;

use humhub\components\ActiveRecord;
use humhub\modules\user\models\Group;
use humhub\modules\user\models\User;
use humhub\modules\yeswiki\models\UserNotExtended;
use humhub\modules\yeswiki\models\Yeswiki;
use Yii;
use yii\base\NotSupportedException;
use yii\db\StaleObjectException;


/**
 * @property string $name
 * @property string $password
 * @property string $email
 * @property string $motto
 * @property int $revisioncount
 * @property int $changescount
 * @property string $doubleclickedit
 * @property string $signuptime
 * @property string $show_comments
 */
class YesWikiUsers extends ActiveRecord
{
    const TABLE_NAME_SUFFIX = 'users';

    /**
     * @var string the associated database table name
     */
    public static $tableName;

    /**
     * @var int HumHub Group ID
     */
    public $humhubGroupId;

    /**
     * @var bool
     */
    public $_tableExists = false;

    /**
     * @inerhitdoc
     */
    public function __construct($config = [])
    {
        // Reinitialize because value is the same as the previous instance
        $this->_tableExists = false;

        // Get table name
        $yeswiki = Yeswiki::findOne($config['humhubGroupId'] ?? $this->humhubGroupId);
        if ($yeswiki === null || !$yeswiki->table_prefix) {
            return;
        }
        static::$tableName = $yeswiki->table_prefix . self::TABLE_NAME_SUFFIX;

        // Check if table exists
        try {
            $tableSchema = static::getDb()
                ->getSchema()
                ->getTableSchema(static::tableName());
        } catch (NotSupportedException $e) {
            return;
        }
        $this->_tableExists = $tableSchema !== null;

        if ($this->_tableExists) {
            parent::__construct($config);
        }
    }

    public static function getDb()
    {
        return Yii::$app->dbYesWiki;
    }

    public static function tableName()
    {
        return static::$tableName;
    }

    /*
        public function attributeLabels()
        {
            return [
            ];
        }

        public function rules()
        {
           return [
           ];
        }
    */


    /**
     * Check if user exists
     * @param Group $humhubGroup
     * @param UserNotExtended $humhubUser
     * @return bool
     */
    public static function userExists($humhubGroup, $humhubUser)
    {
        // Create model with dynamically created table name from humhub group and check if table exists
        $yesWikiUsersModel = new self(['humhubGroupId' => $humhubGroup->id]);
        if (!$yesWikiUsersModel->_tableExists) {
            return false;
        }

        $yesWikiUser = $yesWikiUsersModel::findOne(['name' => $humhubUser->username]);
        return $yesWikiUser !== null;
    }

    /**
     * Add user in YesWiki
     * @param Group $humhubGroup
     * @param UserNotExtended $humhubUser
     * @return bool
     * @throws NotSupportedException
     */
    public static function addUserAndProfile($humhubGroup, $humhubUser)
    {
        if ($humhubUser->status !== User::STATUS_ENABLED) {
            return false;
        }

        $pageTag = $humhubUser->username;

        // Create model with dynamically created table name from humhub group and check if table exists
        $yesWikiUsersModel = new self(['humhubGroupId' => $humhubGroup->id]);
        if (!$yesWikiUsersModel->_tableExists) {
            return false;
        }

        $yesWikiUser = $yesWikiUsersModel::findOne(['name' => $humhubUser->username]);
        if ($yesWikiUser !== null) {
            return false;
        }

        // Create user
        $yesWikiUser = new self(['humhubGroupId' => $humhubGroup->id]);
        $yesWikiUser->name = $humhubUser->username;
        $yesWikiUser->password = 'sso';
        $yesWikiUser->email = $humhubUser->email;
        $yesWikiUser->motto = '';
        $yesWikiUser->revisioncount = 20;
        $yesWikiUser->changescount = 50;
        $yesWikiUser->doubleclickedit = 'Y';
        $yesWikiUser->signuptime = date("Y-m-d H:i:s");
        $yesWikiUser->show_comments = 'N';
        $yesWikiUser->save();

        // Create profile
        YesWikiPages::addOrUpdateProfile($humhubGroup, $humhubUser);

        return true;
    }

    /**
     * Add user in all YesWiki sites
     * @param UserNotExtended $humhubUser
     * @return void
     * @throws NotSupportedException
     */
    public static function addUserAndProfileInAllSites($humhubUser)
    {
        foreach ($humhubUser->syncGroups as $humhubGroup) {
            self::addUserAndProfile($humhubGroup, $humhubUser);
        }
    }

    /**
     * Delete user in YesWiki and all related tags (profile, etc.)
     * Happens when a HumHub user is deleted or removed from a HumHub group
     * @param Group $humhubGroup
     * @param UserNotExtended $humhubUser
     * @return bool
     * @throws NotSupportedException
     * @throws \Throwable
     * @throws StaleObjectException
     */
    public static function deleteUserAndContent($humhubGroup, $humhubUser)
    {
        $pageTag = $humhubUser->username;

        if ($pageTag === 'WikiAdmin') {
            return false;
        }

        // Create model with dynamically created table name from humhub group and check if table exists
        $yesWikiUsersModel = new self(['humhubGroupId' => $humhubGroup->id]);
        if (!$yesWikiUsersModel->_tableExists) {
            return false;
        }

        // Delete user
        $yesWikiUser = $yesWikiUsersModel::findOne(['name' => $pageTag]);
        if ($yesWikiUser !== null) {
            $yesWikiUser->delete();
        }

        // Delete groups member
        foreach ($humhubUser->spaces as $humhubSpace) {
            YesWikiTriples::removeMemberInGroup($humhubGroup, $humhubSpace, $pageTag);
            YesWikiTriples::removeMemberInGroup($humhubGroup, $humhubSpace, $pageTag, true);
        }

        // Delete users pageTag in other tables
        // Pages with owner = $pageTag will not be deleted
        YesWikiPages::deleteTag($humhubGroup, $pageTag);
        YesWikiLinks::deleteTag($humhubGroup, $pageTag);
        YesWikiAcls::deleteTag($humhubGroup, $pageTag);
        YesWikiReferrers::deleteTag($humhubGroup, $pageTag);
        YesWikiTriples::deleteTag($humhubGroup, $pageTag, false);

        return true;
    }

    /**
     * Delete user in all YesWiki sites
     * @param UserNotExtended $humhubUser
     * @return void
     * @throws NotSupportedException
     * @throws \Throwable
     * @throws StaleObjectException
     */
    public static function deleteUserAndContentInAllSites($humhubUser)
    {
        foreach ($humhubUser->syncGroups as $humhubGroup) {
            self::deleteUserAndContent($humhubGroup, $humhubUser);
        }
    }

    /**
     * Delete all users in a YesWiki
     * @param Group $humhubGroup
     * @param bool $keepData If true, in the `triples` table name, delete only the entries related to the properties recreated (especially, keep the entries with property `https://yeswiki.net/vocabulary/progress`)
     * @return bool
     * @throws NotSupportedException
     * @throws \Throwable
     * @throws StaleObjectException
     */
    public static function deleteAllUsers($humhubGroup, $keepData = true)
    {
        // Create model with dynamically created table name from humhub group and check if table exists
        $yesWikiUsersModel = new self(['humhubGroupId' => $humhubGroup->id]);
        if (!$yesWikiUsersModel->_tableExists) {
            return false;
        }

        /** @var self $yesWikiUser */
        foreach (self::find()->each(1000) as $yesWikiUser) {
            $pageTag = $yesWikiUser->name;
            if ($pageTag === 'WikiAdmin') {
                continue;
            }

            // Delete user
            $yesWikiUser->delete();

            // Delete users pageTag in other tables
            // Pages with owner = $pageTag will not be deleted
            YesWikiPages::deleteTag($humhubGroup, $pageTag);
            YesWikiLinks::deleteTag($humhubGroup, $pageTag);
            YesWikiAcls::deleteTag($humhubGroup, $pageTag);
            YesWikiReferrers::deleteTag($humhubGroup, $pageTag);
            YesWikiTriples::deleteTag($humhubGroup, $pageTag, $keepData);
        }

        // Delete all remaining profile forms (in case some entries were create with an old username)
        YesWikiPages::deleteAllProfileEntries($humhubGroup);

        return true;
    }

    /**
     * Update username in YesWiki (correspondance from email) and add user if not exists in all sites
     * @param UserNotExtended $humhubUser
     * @return bool
     * @throws NotSupportedException
     * @throws StaleObjectException
     * @throws \Throwable
     */
    public static function updateUsernameInAllSites($humhubUser)
    {
        if ($humhubUser->status !== User::STATUS_ENABLED) {
            return false;
        }

        foreach ($humhubUser->syncGroups as $humhubGroup) {

            // Create model with dynamically created table name from humhub group and check if table exists
            $yesWikiUsersModel = new self(['humhubGroupId' => $humhubGroup->id]);
            if (!$yesWikiUsersModel->_tableExists) {
                continue;
            }

            // Get user with YesWikiEmail == HumHubEmail
            $yesWikiUser = $yesWikiUsersModel::findOne(['email' => $humhubUser->email]);
            if ($yesWikiUser === null) {
                return false;
            }

            // Create old and new tag to change
            $oldPageTag = $yesWikiUser->name;
            $newPageTag = $humhubUser->username;
            if (empty($oldPageTag) || $oldPageTag == $newPageTag) {
                return false;
            }

            // Update YesWikiUsers table
            $yesWikiUser->name = $humhubUser->username;
            $yesWikiUser->save();

            // Update groups member
            foreach ($humhubUser->spaces as $humhubSpace) {
                YesWikiTriples::updateMemberInGroup($humhubGroup, $humhubSpace, $oldPageTag, $newPageTag);
                YesWikiTriples::updateMemberInGroup($humhubGroup, $humhubSpace, $oldPageTag, $newPageTag, true);
            }

            // Update other tables
            YesWikiPages::updateTag($humhubGroup, $oldPageTag, $newPageTag);
            YesWikiLinks::updateTag($humhubGroup, $oldPageTag, $newPageTag);
            YesWikiAcls::updateTag($humhubGroup, $oldPageTag, $newPageTag);
            YesWikiReferrers::updateTag($humhubGroup, $oldPageTag, $newPageTag);
            YesWikiTriples::updateTag($humhubGroup, $oldPageTag, $newPageTag);
        }

        return true;
    }

    /**
     * @param UserNotExtended $humhubUser
     * @return bool
     * @throws NotSupportedException
     */
    public static function updateEmailInAllSites($humhubUser)
    {
        $username = $humhubUser->username;
        $newEmail = $humhubUser->email;

        foreach ($humhubUser->syncGroups as $humhubGroup) {

            // Create model with dynamically created table name from humhub group and check if table exists
            $yesWikiUsersModel = new self(['humhubGroupId' => $humhubGroup->id]);
            if (!$yesWikiUsersModel->_tableExists) {
                continue;
            }
            // Get YesWiki user from username
            $yesWikiUser = $yesWikiUsersModel::findOne(['name' => $username]);

            if ($yesWikiUser === null) {
                return false;
            }

            if ($yesWikiUser->email == $newEmail) {
                return false;
            }

            // Save new email in table users
            $yesWikiUser->email = $newEmail;
            $yesWikiUser->save();

            // Save new mail in table pages (profile)
            YesWikiPages::updateProfileField($humhubGroup, $username, 'bf_email', $newEmail);
        }

        return true;
    }

    /**
     * @param UserNotExtended $humhubUser
     * @param $newUsername
     * @return bool
     * @throws NotSupportedException
     */
    public static function usernameIsAvailableInAllSites($humhubUser, $newUsername)
    {

        foreach ($humhubUser->syncGroups as $humhubGroup) {

            // Create model with dynamically created table name from humhub group and check if table exists
            $yesWikiUsersModel = new self(['humhubGroupId' => $humhubGroup->id]);
            if (!$yesWikiUsersModel->_tableExists) {
                continue;
            }

            // Create model with dynamically created table name from humhub group and check if table exists
            $yesWikiPagesModel = new YesWikiPages(['humhubGroupId' => $humhubGroup->id]);
            if (!$yesWikiPagesModel->_tableExists) {
                continue;
            }

            // Get YesWiki user from email (in case HumHub's user has already changed username) because if the user exist, it is the current user, and we mustn't return false if his username is the same as the new one
            $yesWikiUser = $yesWikiUsersModel::findOne(['email' => $humhubUser->email]);

            // If no user exists for this email
            if ($yesWikiUser === null) {
                // Check if username available in tables users and pages

                // Table users
                if ($yesWikiUsersModel::findOne(['name' => $newUsername]) !== null) {
                    return false;
                }

                // Table pages
                if ($yesWikiPagesModel::findOne(['tag' => $newUsername]) !== null) {
                    return false;
                }
            } // If user exists for this email
            else {
                // Check if username available in tables users and pages, except the current username
                $currentUsername = $yesWikiUser->name;

                // Table users
                if ($yesWikiUsersModel::find()
                        ->where([
                            'and',
                            ['name' => $newUsername],
                            ['not', ['name' => $currentUsername]],
                        ])
                        ->one() !== null
                ) {
                    return false;
                }

                // Table pages
                if ($yesWikiPagesModel::find()
                        ->where([
                            'and',
                            ['tag' => $newUsername],
                            ['not', ['tag' => $currentUsername]],
                        ])
                        ->one() !== null
                ) {
                    return false;
                }
            }
        }

        return true;
    }
}
