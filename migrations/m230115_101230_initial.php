<?php

use humhub\components\Migration;

/**
 * Class m230115_101230_initial
 */
class m230115_101230_initial extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->safeCreateTable('yeswiki', [
            'group_id' => $this->primaryKey(),
            'table_prefix' => $this->string(127),
            'created_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_at' => $this->dateTime(),
            'updated_by' => $this->integer(11),
        ], '');

        // Add indexes on columns for speeding where operations ; false if values (or values combinaisons if several columns) are not unique
        $this->safeCreateIndex('idx-yeswiki', 'yeswiki', ['group_id'], true);
        // Add foreign keys (if related to a table, when deleted in this table, related rows are deleted to, but beforeDelete() and afterDelete() are not called)
        $this->safeAddForeignKey('fk-yeswiki-group', 'yeswiki', 'group_id', 'group', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m230115_101230_initial cannot be reverted.\n";

        return false;
    }
}
